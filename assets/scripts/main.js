/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */

(function($) {

  // Use this variable to set up the common and page specific functions. If you
  // rename this variable, you will also need to rename the namespace below.
  var Sage = {
    // All pages
    'common': {
      init: function() {
        // JavaScript to be fired on all pages
				$('.home-gallery').matchHeight({
					remove: true,
					target: $('.share-gallery-row')
				});

				/* Magnific */
				$('.img-popup').magnificPopup({
					type: 'image'
					// other options
				});



				$('.slides').each(function() {
					$(this).magnificPopup({
						delegate: 'li a.flex-pop',
						type: 'image',
						image: {
							titleSrc: function(item) {
								return item.el.find('img').attr('title');
							}
						}
					});
				});

				/* Flexslider */
				$(window).load(function() {
					$('.slider').flexslider({
						slideshow: true,
						namespace: "flex-",
						smoothHeight: true,
						animation: "slide",
						controlNav: false,
						animationLoop: true,
						randomize: true,
						sync: "#carousel",
						useCSS: true,
						touch: true,
						directionNav: true,
						easing: "swing"
					});

				});
				/* Flexslider */


			},
      finalize: function() {
        // JavaScript to be fired on all pages, after page specific JS is fired
      }
    },
    // Home page
    'home': {
      init: function() {
        // JavaScript to be fired on the home page


      },
      finalize: function() {
        // JavaScript to be fired on the home page, after the init JS
      }
    },
    // About us page, note the change from about-us to about_us.
    'about_us': {
      init: function() {
        // JavaScript to be fired on the about us page
      }
    }
  };

  // The routing fires all common scripts, followed by the page specific scripts.
  // Add additional events for more control over timing e.g. a finalize event
  var UTIL = {
    fire: function(func, funcname, args) {
      var fire;
      var namespace = Sage;
      funcname = (funcname === undefined) ? 'init' : funcname;
      fire = func !== '';
      fire = fire && namespace[func];
      fire = fire && typeof namespace[func][funcname] === 'function';

      if (fire) {
        namespace[func][funcname](args);
      }
    },
    loadEvents: function() {
      // Fire common init JS
      UTIL.fire('common');

      // Fire page-specific init JS, and then finalize JS
      $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
        UTIL.fire(classnm);
        UTIL.fire(classnm, 'finalize');
      });

      // Fire common finalize JS
      UTIL.fire('common', 'finalize');
    }
  };

  // Load Events
  $(document).ready(UTIL.loadEvents);
  	
  	

  	
  	
  	//----------------------------------------------------------------------------------------	 
 /*-- #zombiecode -----
	//allow wordpress gallery to pop into magnific
		if ($('.gallery-row a')) {
		  $('.gallery-row a').magnificPopup({
			  type:'image',
			  removalDelay: 500,
			  mainClass: 'mfp-fade',
			  gallery:{
				  enabled:true,
				  navigateByImgClick: true,
				  arrowMarkup: '<button title="%title%" type="button" class="mfp-arrow mfp-arrow-%dir%"></button>',
				  tPrev: 'Previous (Left arrow key)',
				  tNext: 'Next (Right arrow key)'
			  }
		  });
		}
-- #zombiecode -----*/
	//----------------------------------------------------------------------------------------	 
	
/*
		// Match Height 
		$('.flex > *').matchHeight();
		// For on gravity form validation 
		$(document).bind('gform_confirmation_loaded', function(){
			$('.flex > *').matchHeight();
		});

*/

	//----------------------------------------------------------------------------------------



	//----------------------------------------------------------------------------------------	 
	/* Magnific */
	
/*-- #zombiecode -----
		$('.popup-video, a[href*="player.vimeo"], a[href*="youtube.com/embed"], a.type_video').magnificPopup({
			type: 'iframe',
			removalDelay: 500,
			mainClass: 'mfp-fade',
		});
		
-- #zombiecode -----*/		
		
	//----------------------------------------------------------------------------------------	 
	/* Magnific */
	
	
/*-- #zombiecode -----	
		$('.popup-page, a[href*="iframe=true"]').magnificPopup({
			type:'iframe',
			removalDelay: 500,
			mainClass: 'mfp-fade',
			closeMarkup: '<button title="Close (Esc)" type="button" class="mfp-close tabbed-close">× Close Window</button>',
			iframe: {
				markup: '<div class="mfp-iframe-scaler tab-frame">'+
				  '<button title="Close (Esc)" type="button" class="mfp-close tabbed-close">× Close Window</button>'+
				  '<iframe class="mfp-iframe" frameborder="0" allowfullscreen></iframe>'+
				'</div>',
				srcAction: 'iframe_src',
			},
		    callbacks: {
			  open: function() {
			    $('iframe').iFrameResize( [{}] );
			  }
		    }
		});
-- #zombiecode -----*/			
			 /* Create button click functionality */
	  $('.search-submit').on('click', function() {
		  	$('input.search-field').toggleClass('show');
	  });

//----------------------------------------------------------------------------------------	 






	//----------------------------------------------------------------------------------------	 
	/* Magnific */
		$('.popup-iframe').magnificPopup({
			type:'iframe',
			mainClass: 'mfp-fade',
			 iframe: {
                    markup: '<div class="mfp-iframe-scaler">'+
                    '<h3><div class="mfp-title"></div></h3>'+
                    '<div class="mfp-close"></div>'+
                    '<iframe class="mfp-iframe" frameborder="0" allowfullscreen></iframe>'+

                    '</div>'
                },
                callbacks: {
                    markupParse: function(template, values, item) {
                        values.title = item.el.attr('title');
                    }
                },
                 gallery:{
                    enabled:true,
                    navigateByImgClick: false,
                    arrowMarkup: '<button title="%title%" type="button" class="mfp-arrow mfp-arrow-%dir%"></button>',

                    tPrev: 'Previous (Left arrow key)',
                    tNext: 'Next (Right arrow key)'
                }

		});
	//----------------------------------------------------------------------------------------	 
	/* Magnific 
		
		
		$('.gallery-iframe').magnificPopup({
			type:'iframe',
			mainClass: 'mfp-fade',
			gallery: false,
			iframe: {
                markup: '<div class="mfp-iframe-scaler">'+
                '<h3><div class="mfp-title"></div></h3>'+
                '<div class="mfp-close"></div>'+
                '<iframe class="mfp-iframe" frameborder="0" allowfullscreen></iframe>'+

                '</div>'
            }
		});
		*/

		//--------------------------------------------
		
		/*
		$('.slides').each(function() {
		    $(this).magnificPopup({
		        delegate: 'a.gallery-iframe',
		        type: 'iframe',
		        gallery: {
		            enabled: true,
		            navigateByImgClick: true,
		            tCounter: '%curr% of %total%', // Markup for "1 of 7" counter
		            preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
		        },
		        image: {
		            titleSrc: function(item) {
		                return item.el.find('img').attr('title');
		            }
		        }
		    });
		});
	*/

		
		
//----------------------------------------------------------------------------------------	  	

//----------------------------------------------------------------------------------------	
//----------------------------------------------------------------------------------------	  	
/* Move Nav button inside the header the header 
- MDL auto creates the nav button - there is no template representation of the button so this is how we move it.
*/


	$('.mdl-layout__drawer-button').addClass('aStyle');
	
	
	

	
	

})(jQuery); // Fully reference jQuery after this point.
