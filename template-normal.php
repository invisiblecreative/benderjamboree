<?php
/**
 * Template Name: Normal Page Template
 */
?>

<?php while (have_posts()) : the_post(); ?> 
<div class="mdl-grid em-home wt-bkg mdl-shadow--6dp">
	<div class="mdl-cell mdl-cell--12-col">		
		<h2 class=""><?php the_title(); ?></h2>			
		<?php the_content(); ?>
	</div>
</div>  
<?php endwhile; ?>
