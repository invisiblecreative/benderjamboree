<?php
  /**
   * Template Name: Rewind List
   */
?>


<?php while (have_posts()) : the_post(); ?>
  <?php if(get_the_content() != ''){?>
    <div class="mdl-grid wt-bkg rewind-page-content mdl-shadow--6dp">
      <div class="mdl-cell mdl-cell--12-col">
        <article <?php post_class(); ?>>
          <header>
            <h1 class="entry-title"><?php the_title(); ?></h1>
          </header>
          <div class="entry-content">
            <?php the_content(); ?>
          </div>
        </article>
      </div>
    </div>
  <?php } else{ ?>
    <div class="mdl-grid">
      <div class="mdl-cell mdl-cell--12-col">
        <article <?php post_class(); ?>>
          <header>
            <h1 class="entry-title"><?php the_title(); ?></h1>
          </header>
        </article>
      </div>
    </div>
  <?php }?>
<?php endwhile; ?>










<?php
  $rewindLoop = new WP_Query(
    array(
      'post_type' => 'event_detail',
      'ignore_sticky_posts' => 1,
      'paged' => $paged,
      'tax_query' => array(
        array(
          'taxonomy' => 'event_status',
          'field'    => 'term_id',
          'terms'    => '2',
          'operator' => 'NOT IN'
        ),
      ),
      'orderby' => 'title',
      'order'   => 'DESC',
    )
  );
  if ( $rewindLoop->have_posts() ) :
    while ( $rewindLoop->have_posts() ) : $rewindLoop->the_post();
      $rewindID = get_the_ID();
      ?>


      <div id="rewind_<?php echo $rewindID ?>" class="mdl-grid em-home em-rewind row wt-bkg mdl-shadow--6dp">
          <div class="rewind-welcome mdl-cell mdl-cell--6-col  share-gallery-row">
            <div class="ptitle mdl-cell mdl-cell--12-col">
              <h2><a href="<?php echo get_permalink(); ?>"><?php echo get_the_title(); ?></a></h2>
            </div>
            <div class="rewind-excerpt">
              <?php
                $excerpt = wp_trim_words( get_field('em_welcome_copy', $eventID), $num_words = 75, $more = ' ... ' );
                echo $excerpt;
              ?>
            </div>
            <div class="rewind-link ">
              <div class="rewindBtn mdl-shadow--2dp">
                <a class="book-btn button btn rewindLink" href="<?php echo get_permalink(); ?>">View the <?php echo get_the_title(); ?> Rewind</a>
              </div>
            </div>
          </div>
          <div class="mdl-cell mdl-cell--6-col share-gallery-row">
            <?php if(get_field('em_section1_lead', $eventID) === 'featurePkg'){
              get_template_part('templates/em-parts/em', 'feature');
            } else { ?>
              <div class="share-card-image mdl-card mdl-shadow--2dp <?php if (get_field('em_section1_lead', $eventID) === 'video'){echo 'embed-container'; }?>">
                <?php if (get_field('em_section1_lead', $eventID) === 'video'){ ?>

                  <?php the_field('em_feature_video', $eventID); ?>

                <?php } else { ?>
                  <a class="img-popup" href="<?php the_field('em_share_poster', $eventID); ?>">
                    <img src="<?php the_field('em_share_poster', $eventID); ?>" />
                  </a>
                <?php }?>
              </div>
            <?php } ?>
          </div>
      </div>
    <?php endwhile; ?>
    <?php
    if (  $rewindLoop->max_num_pages > 1 ) : ?>
      <div id="nav-below" class="navigation">
        <div class="nav-previous"><?php next_posts_link( __( '<span class="meta-nav">&larr;</span> Previous', 'domain' ) ); ?></div>
        <div class="nav-next"><?php previous_posts_link( __( 'Next <span class="meta-nav">&rarr;</span>', 'domain' ) ); ?></div>
      </div>
    <?php endif;
  endif;
  wp_reset_postdata();
?>




