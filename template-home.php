<?php
/**
 * Template Name: Home Template
 */
?>
<?php while (have_posts()) : the_post();
  get_template_part('templates/em-parts/em', 'home');
  get_template_part('templates/em-parts/em', 'artists');
  get_template_part('templates/em-parts/em', 'package');
  get_template_part('templates/em-parts/em', 'addons');
 endwhile; ?>
