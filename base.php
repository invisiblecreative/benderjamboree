<?php

use Roots\Sage\Config;
use Roots\Sage\Wrapper;

get_template_part('templates/emid');


?>
 
<!doctype html>
<html class="no-js" <?php language_attributes(); ?>>
  <?php get_template_part('templates/head'); ?>
  <body <?php body_class(); ?>>
    <!--[if lt IE 9]>
      <div class="alert alert-warning">
        <?php _e('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'sage'); ?>
      </div>
    <![endif]-->
    <?php get_template_part('templates/em-parts/em', 'openbody'); ?>
	<div class="mdl-layout__content mdl-js-layout">  
	            <?php
          do_action('get_header');
          get_template_part('templates/header');
        ?>
        

        <div class="mdl-layout" role="document">
			<div class="mdl-grid">
	            <main class="mdl-cell mdl-cell--12-col" role="main">
		          
	              <?php include Wrapper\template_path(); ?>
	             
	            </main><!-- main -->
			</div><!-- /.page-content.mdl-grid -->
          <?php
            do_action('get_footer');
            get_template_part('templates/footer');
            wp_footer();
          ?>
        </div><!-- /.mdl-layout__content -->
      </div><!-- /.mdl-layout -->
    </div>
  </body>
</html>
