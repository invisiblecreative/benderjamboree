<?php
/**
 * Template Name: Packages Template
 */
?>

<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/em-parts/em', 'package'); ?>
  <?php get_template_part('templates/em-parts/em', 'addons'); ?>
<?php endwhile; ?>
