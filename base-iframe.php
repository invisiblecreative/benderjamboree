<?php

use Roots\Sage\Config;
use Roots\Sage\Wrapper;

?>

<?php get_template_part('templates/head'); ?>
  <body <?php body_class('iframe-layout'); ?>>
  	<?php get_template_part('templates/body-open'); ?>
    <main class="col-m-12" role="main">
		<?php include Wrapper\template_path(); ?>
	</main><!-- /.main -->
    <?php get_template_part('templates/body-close'); ?>
    <?php wp_footer();?>
  </body>
</html>
