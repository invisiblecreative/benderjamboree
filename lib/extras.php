<?php

namespace Roots\Sage\Extras;

use Roots\Sage\Config;

/**
 * Add <body> classes
 */
function body_class($classes) {
  // Add page slug if it doesn't exist
  if (is_single() || is_page() && !is_front_page()) {
    if (!in_array(basename(get_permalink()), $classes)) {
      $classes[] = basename(get_permalink());
    }
  }

  // Add class if sidebar is active
  if (Config\display_sidebar()) {
    $classes[] = 'sidebar-primary';
  }

  return $classes;
}
add_filter('body_class', __NAMESPACE__ . '\\body_class');

/**
 * Clean up the_excerpt()
 */
function excerpt_more() {
  return ' &hellip; <a href="' . get_permalink() . '">' . __('Continued', 'sage') . '</a>';
}
add_filter('excerpt_more', __NAMESPACE__ . '\\excerpt_more');

 
// OS and browser detection classes
function mv_browser_body_class($classes) {
        global $is_lynx, $is_gecko, $is_IE, $is_opera, $is_NS4, $is_safari, $is_chrome, $is_iphone;
        if($is_lynx) $classes[] = 'lynx';
        elseif($is_gecko) $classes[] = 'gecko';
        elseif($is_opera) $classes[] = 'opera';
        elseif($is_NS4) $classes[] = 'ns4';
        elseif($is_safari) $classes[] = 'safari';
        elseif($is_chrome) $classes[] = 'chrome';
        elseif($is_IE) {
                $classes[] = 'ie';
                if(preg_match('/MSIE ([0-9]+)([a-zA-Z0-9.]+)/', $_SERVER['HTTP_USER_AGENT'], $browser_version))
                $classes[] = 'ie'.$browser_version[1];
        } else $classes[] = 'unknown';
        if($is_iphone) $classes[] = 'iphone';
        if ( stristr( $_SERVER['HTTP_USER_AGENT'],"mac") ) {
                 $classes[] = 'osx';
           } elseif ( stristr( $_SERVER['HTTP_USER_AGENT'],"linux") ) {
                 $classes[] = 'linux';
           } elseif ( stristr( $_SERVER['HTTP_USER_AGENT'],"windows") ) {
                 $classes[] = 'windows';
           }
        return $classes;
}
add_filter('body_class', __NAMESPACE__ . '\\mv_browser_body_class');

// Use Bootstrap Responsive Embed for videos in content
function bootstrap_wrap_oembed( $html ){
  $html = preg_replace( '/(width|height)="\d*"\s/', "", $html );
  return'<div class="embed-responsive embed-responsive-16by9">'.$html.'</div>';
}
add_filter( 'embed_oembed_html', __NAMESPACE__ . '\\bootstrap_wrap_oembed',10,1);


// Add logo and url to login page from theme options
function my_login_logo_one() { 
?> 
<style type="text/css"> 
body.login div#login h1 a {
 background-image: url(<?php if ( get_field('to_logo', 'option') ) { the_field('to_logo', 'option'); } else { bloginfo('name'); }; ?>); background-size:contain; width:100%; height:150px; } 
</style>
 <?php 
}
add_action( 'login_enqueue_scripts', __NAMESPACE__ . '\\my_login_logo_one' );

function my_login_logo_url() {
    return home_url();
}
add_filter( 'login_headerurl', __NAMESPACE__ . '\\my_login_logo_url' );

function my_login_logo_url_title() {
	$blogname = get_bloginfo('name');
    return $blogname;
}
add_filter( 'login_headertitle', __NAMESPACE__ . '\\my_login_logo_url_title' );

/* #zombiecode 
// Block Robots for dev enviroment 
if( WP_ENV == 'development' ) {
    if ( get_option('blog_public') == 1 ) {
        update_option( 'blog_public', 0 );
    }
} else {
    if ( get_option('blog_public') == 0 ) {
        update_option( 'blog_public', 1 );
    }
}
*/
/* Add SVG to uploads */
function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', __NAMESPACE__ . '\\cc_mime_types'); 

//* Make link button checkbox
function overwrite_wplink() {
	// Disable wplink
	wp_deregister_script( 'wplink' );

	// Register a new script file to be linked
	wp_register_script( 'wplink', get_stylesheet_directory_uri() . '/assets/scripts/wplink.min.js', array( 'jquery', 'wpdialogs' ), false, 1 );
}
add_action( 'admin_enqueue_scripts', __NAMESPACE__ . '\\overwrite_wplink', 999 );


add_filter('relevanssi_excerpt_content', __NAMESPACE__ . '\\custom_fields_to_excerpts', 10, 3);
function custom_fields_to_excerpts($content, $post, $query) {
$custom_field = get_post_meta($post->ID, 'pkg_add_on_long_desc', true);
$content .= " " . $custom_field;
$custom_field = get_post_meta($post->ID, 'artist_description', true);
$content .= " " . $custom_field;
$custom_field = get_post_meta($post->ID, 'pkg_description', true);
$content .= " " . $custom_field;
$custom_field = get_post_meta($post->ID, 'pkg_location', true);
$content .= " " . $custom_field;
$custom_field = get_post_meta($post->ID, 'bedding_preference', true);
$content .= " " . $custom_field;
$custom_field = get_post_meta($post->ID, 'pkg_smoking_preference', true);
$content .= " " . $custom_field;
$custom_field = get_post_meta($post->ID, 'bedding_preference', true);
$content .= " " . $custom_field;
$custom_field = get_post_meta($post->ID, 'bedding_preference', true);
$content .= " " . $custom_field;
    
$fields = get_field('pkg_includes', $post->ID);
if($fields){
	foreach($fields as $field){
		$content .= " " . $field['pkc_include'];
	}
}
$fields = get_field('pkg_notices', $post->ID);
if($fields){
	foreach($fields as $field){
		$content .= " " . $field['pkg_notice'];
	}
}
$fields = get_field('em_milestones', $post->ID);
if($fields){
	foreach($fields as $field){
		$content .= " " . $field['em_milestone'];
		$content .= " " . $field['em_milestone_date'];
	}
}
$fields = get_field('faq_set', $post->ID);
if($fields){
	foreach($fields as $field){
		$content .= " " . $field['faq_cat_title'];
		$cat_sets = get_field('faq_cat_set', $post->ID);
		if($cat_sets){
			foreach($cat_sets as $cat_set){
				$content .= " " . $field['faq_question'];
				$content .= " " . $field['faq_answer'];
			}
		}		
	}
}



return $content;
}

add_filter( 'jetpack_enable_opengraph', '__return_false', 99 );