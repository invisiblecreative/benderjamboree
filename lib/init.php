<?php

namespace Roots\Sage\Init;

use Roots\Sage\Assets;

/**
 * Theme setup
 */
function setup() {
  // Make theme available for translation
  // Community translations can be found at https://github.com/roots/sage-translations
  load_theme_textdomain('sage', get_template_directory() . '/lang');

  // Enable plugins to manage the document title
  // http://codex.wordpress.org/Function_Reference/add_theme_support#Title_Tag
  add_theme_support('title-tag');

// Register wp_nav_menu() menus
  // http://codex.wordpress.org/Function_Reference/register_nav_menus
  register_nav_menus([
    'top_bar' => __('Top Bar', 'sage'),
    'primary_navigation' => __('Primary Navigation', 'sage'),
    'footer_navigation' => __('Footer Navigation', 'sage'),
    'sub_footer_navigation' => __('Sub Footer Navigation', 'sage'),
    'sitemap' => __('Sitemap', 'sage')
  ]);


  // Add post thumbnails
  // http://codex.wordpress.org/Post_Thumbnails
  // http://codex.wordpress.org/Function_Reference/set_post_thumbnail_size
  // http://codex.wordpress.org/Function_Reference/add_image_size
  add_theme_support('post-thumbnails');

  // Add post formats
  // http://codex.wordpress.org/Post_Formats
  add_theme_support('post-formats', ['aside', 'gallery', 'link', 'image', 'quote', 'video', 'audio']);

  // Add HTML5 markup for captions
  // http://codex.wordpress.org/Function_Reference/add_theme_support#HTML5
  add_theme_support('html5', ['caption', 'comment-form', 'comment-list']);

  // Tell the TinyMCE editor to use a custom stylesheet
  add_editor_style(Assets\asset_path('styles/editor-style.css'));
}
add_action('after_setup_theme', __NAMESPACE__ . '\\setup');

/**
 * Register sidebars
 */
function widgets_init() {
  register_sidebar([
    'name'          => __('Primary', 'sage'),
    'id'            => 'sidebar-primary',
    'before_widget' => '<section class="widget %1$s %2$s">',
    'after_widget'  => '</section>',
    'before_title'  => '<h3>',
    'after_title'   => '</h3>'
  ]);

  register_sidebar([
    'name'          => __('Mini Footer', 'sage'),
    'id'            => 'sidebar-mini-footer',
    'before_widget' => '<div class="mdl-mini-footer__left-section">',
    'after_widget'  => '</div>',
    'before_title'  => '<div class="mdl-logo">',
    'after_title'   => '</div>'
  ]);

  register_sidebar([
    'name'          => __('Mega Footer', 'sage'),
    'id'            => 'sidebar-mega-footer',
    'before_title'  => '</div><input class="mdl-mega-footer--heading-checkbox" type="checkbox" checked><h2 class="widget-title mdl-mega-footer--heading">',
    'after_title'   => '</h2><div class="mdl-mega-footer--link-list">',
    'before_widget' => '<section class="mdl-mega-footer__drop-down-section"><div>',
    'after_widget'  => '</div></section>',
  ]);
}
add_action('widgets_init', __NAMESPACE__ . '\\widgets_init');



// Create optons page for ACF theme options.
//---------------------------------------------------------------
if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Site General Settings',
		'menu_title'	=> 'Site Settings', 
		'menu_slug' 	=> 'site-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
		
}



// Adding custom post type EVENT DETAIL
//---------------------------------------------------------------
if ( ! function_exists('custom_post_type_event') ) {

// Register Custom Post Type
function custom_post_type_event() {

	$labels = array(
		'name'                  => _x( 'Events', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'Event Detail', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'Event Detail', 'text_domain' ),
		'name_admin_bar'        => __( 'Events', 'text_domain' ),
		'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
		'all_items'             => __( 'All Events', 'text_domain' ),
		'add_new_item'          => __( 'Add New Event', 'text_domain' ),
		'add_new'               => __( 'Add New Event ', 'text_domain' ),
		'new_item'              => __( 'New Event', 'text_domain' ),
		'edit_item'             => __( 'Edit Event Detail', 'text_domain' ),
		'update_item'           => __( 'Update Event', 'text_domain' ),
		'view_item'             => __( 'View Event Info', 'text_domain' ),
		'search_items'          => __( 'Search Event', 'text_domain' ),
		'not_found'             => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
		'items_list'            => __( 'Events list', 'text_domain' ),
		'items_list_navigation' => __( 'Events list navigation', 'text_domain' ),
		'filter_items_list'     => __( 'Filter events list', 'text_domain' ),
	);
	$rewrite = array(
		'slug'                  => 'event-detail',
		'with_front'            => true,
		'pages'                 => true,
		'feeds'                 => true,
	);
	$args = array(
		'label'                 => __( 'Event Detail', 'text_domain' ),
		'description'           => __( 'Anual Event Detail post type.', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', 'revisions', 'custom-fields', 'page-attributes', ),
		'taxonomies'            => array( 'event_status' ),
		'hierarchical'          => true,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-index-card',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,		
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'rewrite'               => $rewrite,
		'capability_type'       => 'page',
	);
	register_post_type( 'event_detail', $args );

}
add_action( 'init', __NAMESPACE__ . '\\custom_post_type_event', 0 );

}

// Custom Taxonomy for Events
//------------------------------------------------------------------------------
if ( ! function_exists( 'custom_tax_event_status' ) ) {

// Register Custom Taxonomy
function custom_tax_event_status() {

	$labels = array(
		'name'                       => _x( 'Event Status', 'Taxonomy General Name', 'text_domain' ),
		'singular_name'              => _x( 'Event Status', 'Taxonomy Singular Name', 'text_domain' ),
		'menu_name'                  => __( 'Event Status', 'text_domain' ),
		'all_items'                  => __( 'All Statuses', 'text_domain' ),
		'parent_item'                => __( 'Event Detail', 'text_domain' ),
		'parent_item_colon'          => __( 'Event Detail:', 'text_domain' ),
		'new_item_name'              => __( 'New Status Name', 'text_domain' ),
		'add_new_item'               => __( 'Add New Status', 'text_domain' ),
		'edit_item'                  => __( 'Edit Status', 'text_domain' ),
		'update_item'                => __( 'Update Status', 'text_domain' ),
		'view_item'                  => __( 'View Status', 'text_domain' ),
		'separate_items_with_commas' => __( 'Separate statuses with commas', 'text_domain' ),
		'add_or_remove_items'        => __( 'Add or remove status', 'text_domain' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'text_domain' ),
		'popular_items'              => __( 'Popular Statuses', 'text_domain' ),
		'search_items'               => __( 'Search Statuses', 'text_domain' ),
		'not_found'                  => __( 'Not Found', 'text_domain' ),
		'items_list'                 => __( 'Status list', 'text_domain' ),
		'items_list_navigation'      => __( 'Status list navigation', 'text_domain' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => false,
	);
	register_taxonomy( 'event_status', array( 'event_detail' ), $args );

}
add_action( 'init', __NAMESPACE__ . '\\custom_tax_event_status', 0 );

}


// Custom Post Type for Artists
//------------------------------------------------------------------------------
if ( ! function_exists('custom_post_type_artists') ) {

// Register Custom Post Type
function custom_post_type_artists() {

	$labels = array(
		'name'                  => __( 'Artists', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => __( 'Artist', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'Artists', 'text_domain' ),
		'name_admin_bar'        => __( 'Artists', 'text_domain' ),
		'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
		'all_items'             => __( 'All Artists', 'text_domain' ),
		'add_new_item'          => __( 'Add New Artist', 'text_domain' ),
		'add_new'               => __( 'Add Artist', 'text_domain' ),
		'new_item'              => __( 'New Artist', 'text_domain' ),
		'edit_item'             => __( 'Edit Artist', 'text_domain' ),
		'update_item'           => __( 'Update Artist', 'text_domain' ),
		'view_item'             => __( 'View Artist', 'text_domain' ),
		'search_items'          => __( 'Search Artists', 'text_domain' ),
		'not_found'             => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
		'items_list'            => __( 'Artist list', 'text_domain' ),
		'items_list_navigation' => __( 'Artists list navigation', 'text_domain' ),
		'filter_items_list'     => __( 'Filter Artists list', 'text_domain' ),
	);
	$rewrite = array(
		'slug'                  => 'artists',
		'with_front'            => true,
		'pages'                 => true,
		'feeds'                 => false,
	);
	$args = array(
		'label'                 => __( 'Artist', 'text_domain' ),
		'description'           => __( 'Artists content.', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'excerpt', 'thumbnail', 'revisions', 'custom-fields', 'page-attributes', ),
		'taxonomies'            => array( 'line_ups' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-format-audio',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => false,		
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'query_var'             => 'post_artist',
		'rewrite'               => $rewrite,
		'capability_type'       => 'post',
	);
	register_post_type( 'artists_post_type', $args );

}
add_action( 'init', __NAMESPACE__ . '\\custom_post_type_artists', 0 );

}

// Custom Taxonomy for Artist
//------------------------------------------------------------------------------
if ( ! function_exists( 'custom_tax_line_up' ) ) {

// Register Custom Taxonomy
function custom_tax_line_up() {

	$labels = array(
		'name'                       => _x( 'Line Up', 'Taxonomy General Name', 'text_domain' ),
		'singular_name'              => _x( 'Line Up', 'Taxonomy Singular Name', 'text_domain' ),
		'menu_name'                  => __( 'Line Up', 'text_domain' ),
		'all_items'                  => __( 'All Line Ups', 'text_domain' ),
		'parent_item'                => __( 'Parent Item', 'text_domain' ),
		'parent_item_colon'          => __( 'Parent Item:', 'text_domain' ),
		'new_item_name'              => __( 'New Line Up Name', 'text_domain' ),
		'add_new_item'               => __( 'Add New Line Up', 'text_domain' ),
		'edit_item'                  => __( 'Edit Line Up', 'text_domain' ),
		'update_item'                => __( 'Update Line Up', 'text_domain' ),
		'view_item'                  => __( 'View Line Up', 'text_domain' ),
		'separate_items_with_commas' => __( 'Separate line_ups with commas', 'text_domain' ),
		'add_or_remove_items'        => __( 'Add or remove line_ups', 'text_domain' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'text_domain' ),
		'popular_items'              => __( 'Popular Line Ups', 'text_domain' ),
		'search_items'               => __( 'Search Line Ups', 'text_domain' ),
		'not_found'                  => __( 'Not Found', 'text_domain' ),
		'items_list'                 => __( 'Line Up list', 'text_domain' ),
		'items_list_navigation'      => __( 'Line Ups list navigation', 'text_domain' ),
	);
	$rewrite = array(
		'slug'                       => 'line_ups',
		'with_front'                 => true,
		'hierarchical'               => false,
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
		'query_var'                  => 'line_ups',
		'rewrite'                    => $rewrite,
	);
	register_taxonomy( 'line_up', array( 'artists_post_type' ), $args );

}
add_action( 'init', __NAMESPACE__ . '\\custom_tax_line_up', 0 );

}


// Custom Post Type for Packages
//------------------------------------------------------------------------------

if ( ! function_exists('custom_post_type_packages') ) {

// Register Custom Post Type
function custom_post_type_packages() {

	$labels = array(
		'name'                  => __( 'Packages', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => __( 'Package', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'Packages', 'text_domain' ),
		'name_admin_bar'        => __( 'Packages', 'text_domain' ),
		'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
		'all_items'             => __( 'All Items', 'text_domain' ),
		'add_new_item'          => __( 'Add New Package', 'text_domain' ),
		'add_new'               => __( 'Add Package', 'text_domain' ),
		'new_item'              => __( 'New Package', 'text_domain' ),
		'edit_item'             => __( 'Edit Package', 'text_domain' ),
		'update_item'           => __( 'Update Package', 'text_domain' ),
		'view_item'             => __( 'View Package', 'text_domain' ),
		'search_items'          => __( 'Search Packages', 'text_domain' ),
		'not_found'             => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
		'items_list'            => __( 'Packages list', 'text_domain' ),
		'items_list_navigation' => __( 'Packages list navigation', 'text_domain' ),
		'filter_items_list'     => __( 'Filter Packages list', 'text_domain' ),
	);
	$rewrite = array(
		'slug'                  => 'package',
		'with_front'            => true,
		'pages'                 => true,
		'feeds'                 => false,
	);
	$args = array(
		'label'                 => __( 'Package', 'text_domain' ),
		'description'           => __( 'Packages content.', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'excerpt', 'thumbnail', 'revisions', 'custom-fields', 'page-attributes', ),
		'taxonomies'            => array( 'package_type' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-tickets-alt',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => false,		
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'query_var'             => 'post_package',
		'rewrite'               => $rewrite,
		'capability_type'       => 'post',
	);
	register_post_type( 'packages_post_type', $args );

}
add_action( 'init', __NAMESPACE__ . '\\custom_post_type_packages', 0 );

}



// Custom Taxonomy for Packages
//------------------------------------------------------------------------------
if ( ! function_exists( 'custom_tax_package_type' ) ) {

// Register Custom Taxonomy
function custom_tax_package_type() {

	$labels = array(
		'name'                       => _x( 'Package Types', 'Taxonomy General Name', 'text_domain' ),
		'singular_name'              => _x( 'Package Type', 'Taxonomy Singular Name', 'text_domain' ),
		'menu_name'                  => __( 'Package Type', 'text_domain' ),
		'all_items'                  => __( 'All Package Types', 'text_domain' ),
		'parent_item'                => __( 'Parent Item', 'text_domain' ),
		'parent_item_colon'          => __( 'Parent Item:', 'text_domain' ),
		'new_item_name'              => __( 'New Package Type Name', 'text_domain' ),
		'add_new_item'               => __( 'Add New Package Type', 'text_domain' ),
		'edit_item'                  => __( 'Edit Package Type', 'text_domain' ),
		'update_item'                => __( 'Update Package Type', 'text_domain' ),
		'view_item'                  => __( 'View Package Type', 'text_domain' ),
		'separate_items_with_commas' => __( 'Separate Package Types with commas', 'text_domain' ),
		'add_or_remove_items'        => __( 'Add or remove Package Types', 'text_domain' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'text_domain' ),
		'popular_items'              => __( 'Popular Package Types', 'text_domain' ),
		'search_items'               => __( 'Search Package Types', 'text_domain' ),
		'not_found'                  => __( 'Not Found', 'text_domain' ),
		'items_list'                 => __( 'Package Type list', 'text_domain' ),
		'items_list_navigation'      => __( 'Package Type list navigation', 'text_domain' ),
	);
	$rewrite = array(
		'slug'                       => 'package-type',
		'with_front'                 => true,
		'hierarchical'               => false,
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
		'query_var'                  => 'packgage_type',
		'rewrite'                    => $rewrite,
	);
	register_taxonomy( 'package_type', array( 'packages_post_type' ), $args );

}
add_action( 'init', __NAMESPACE__ . '\\custom_tax_package_type', 0 );

}
if ( ! function_exists('custom_post_type_pkg_addon') ) {

// Register Custom Post Type
function custom_post_type_pkg_addon() {

	$labels = array(
		'name'                  => _x( 'Add Ons', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'Add On', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'Add Ons', 'text_domain' ),
		'name_admin_bar'        => __( 'Add Ons', 'text_domain' ),
		'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
		'all_items'             => __( 'All Add Ons', 'text_domain' ),
		'add_new_item'          => __( 'Add New Add On', 'text_domain' ),
		'add_new'               => __( 'Add New ', 'text_domain' ),
		'new_item'              => __( 'New Add On', 'text_domain' ),
		'edit_item'             => __( 'Edit Add On', 'text_domain' ),
		'update_item'           => __( 'Update Add On', 'text_domain' ),
		'view_item'             => __( 'View Add On', 'text_domain' ),
		'search_items'          => __( 'Search Add On', 'text_domain' ),
		'not_found'             => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
		'items_list'            => __( 'Add Ons list', 'text_domain' ),
		'items_list_navigation' => __( 'Add Ons list navigation', 'text_domain' ),
		'filter_items_list'     => __( 'Filter Add Ons list', 'text_domain' ),
	);
	$args = array(
		'label'                 => __( 'Add On', 'text_domain' ),
		'description'           => __( 'Add ons for packages.', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'excerpt', 'thumbnail', 'revisions', ),
		'taxonomies'            => array( 'category', 'post_tag' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-products',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => false,		
		'exclude_from_search'   => true,
		'publicly_queryable'    => true,
		'query_var'             => 'pkg_addon',
		'capability_type'       => 'post',
	);
	register_post_type( 'pkg_addon', $args );

}
add_action( 'init', __NAMESPACE__ . '\\custom_post_type_pkg_addon', 0 );

}