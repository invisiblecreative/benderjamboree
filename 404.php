<div class="mdl-grid wt-bkg entry-content">
    <div class="mdl-cell mdl-cell--12-col">
        <?php get_template_part('templates/page', 'header'); ?>
    </div>
    <div class="mdl-cell mdl-cell--12-col">
        <?php if (!have_posts()) : ?>
            <div class="alert alert-warning">
                <?php _e('Sorry, but the page you were trying to view does not exist.', 'sage'); ?>
            </div>
            <div class="search-form-wrap">
                <?php get_template_part('templates/searchform'); ?>
            </div>
        <?php endif; ?>
    </div>
</div>

