<?php
/**
 * Template Name: Milestones Template
 */
?>

<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/single', 'milestone'); ?>
<?php endwhile; ?>
