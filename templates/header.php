<?php use Roots\Sage\Nav\NavWalker;

// For getting post ID on every page. #zombiecode
//include('page-id.php');
?>

<div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">

	<header class="mdl-layout__header mdl-layout__header--transparent">
		
		<div class="mdl-layout__header-row">
			<!-- logo -->
			<div id="mobile-Logo">
				<a href="<?= esc_url(home_url('/')); ?>">
					<img src="	
					<?php 
						if(get_field('em_logo_mobile', $GLOBALS['emid'] )){
							the_field('em_logo_mobile', $GLOBALS['emid'] ); 
						}else{
							the_field('default_logo_mobile', 'option' );
						};
					
					?>" />
				</a>
			</div>			<!-- Add spacer, to align navigation to the right -->
			<div class="mdl-layout-spacer"></div>
			<!-- Navigation -->
			<?php
				$menuChoice = get_field('em_menu_display', $GLOBALS['emid']);

				if ( $menuChoice != '' ) :
			
			  // Remove wrapping <li> from around links
			  // https://css-tricks.com/snippets/wordpress/remove-li-elements-from-output-of-wp_nav_menu/#comment-542093
			  $cleanermenu = wp_nav_menu( array(
			    'menu' => $menuChoice,
			    'container' => false,
			    'items_wrap' => '<nav class="mdl-navigation">%3$s</nav>',
			    'echo' => false,
			    'depth' => 2,
			  ) );
			  //$find = array('><a','<li');
			  //$replace = array('','<a');
			  echo str_replace( $find, $replace, $cleanermenu );
			else :
				// Remove wrapping <li> from around links
				// https://css-tricks.com/snippets/wordpress/remove-li-elements-from-output-of-wp_nav_menu/#comment-542093
				$cleanermenu = wp_nav_menu( array(
								'theme_location' => 'primary_navigation',
								'container' => false,
								'items_wrap' => '<nav class="mdl-navigation">%3$s</nav>',
								'echo' => false,
								'depth' => 2,
				) );
				//$find = array('><a','<li');
				//$replace = array('','<a');
				echo str_replace( $find, $replace, $cleanermenu );
			endif;
			?>
			<div class="search-form-wrap">
				<?php get_template_part('templates/headersearchform'); ?>
			</div>
		</div>
	</header>

	<div class="mdl-layout__drawer">
		

		<div id="drawer-mobile-Logo">
			<a href="<?= esc_url(home_url('/')); ?>" style="background-image:url(<?php 
					if(get_field('em_logo_mobile', $GLOBALS['emid'] )){
						the_field('em_logo_mobile', $GLOBALS['emid'] ); 
					}else{
						the_field('default_logo_mobile', 'option' );
					};
				
				?>);">
			</a>
		</div>			
	
	  <?php
			if ( $menuChoice != '' ) :
	
	      // Remove wrapping <li> from around links
			  // https://css-tricks.com/snippets/wordpress/remove-li-elements-from-output-of-wp_nav_menu/#comment-542093
			  $cleanermenu = wp_nav_menu( array(
			    'menu' => $menuChoice,
			    'container' => false,
			    'items_wrap' => '<nav class="mdl-navigation">%3$s</nav>',
			    'echo' => false,
			    'depth' => 1,
			  ) );
			  $find = array('><a','<li');
			  $replace = array('','<a');
			  echo str_replace( $find, $replace, $cleanermenu );
			else :
				// Remove wrapping <li> from around links
				// https://css-tricks.com/snippets/wordpress/remove-li-elements-from-output-of-wp_nav_menu/#comment-542093
				$cleanermenu = wp_nav_menu( array(
								'theme_location' => 'primary_navigation',
								'container' => false,
								'items_wrap' => '<nav class="mdl-navigation">%3$s</nav>',
								'echo' => false,
								'depth' => 1,
				) );
				$find = array('><a','<li');
				$replace = array('','<a');
				echo str_replace( $find, $replace, $cleanermenu );
	  endif;
	  ?>
	  <div class="search-form-wrap">
			<?php get_template_part('templates/searchform'); ?>
		</div>
	  <div id="drawer-mobile-social">
	  <?php get_template_part('templates/social'); ?>
	  </div>
</div>	
	</div>
	<div class="mdl-layout">	
		<div class="banner-wrap">
	    	<a class="em-layout-transparent hero-banner" style="background-image: url(
				<?php
					$eventID = $GLOBALS['emid'];
					if(is_singular('event_detail')){
						$eventID = get_the_ID();
					}
				?>
				<?php if(get_field('em_hero', $eventID )){ ?>
					<?php the_field('em_hero', $eventID ) ?>
				<?php } elseif ( get_field('em_hero', $GLOBALS['emid'] )){ ?>
					<?php the_field('em_hero',$GLOBALS['emid'] ) ?>
				<?php } else { ?>
					<?php the_field('default_header_img', 'option' ); ?>
				<?php } ?>
				);" href="<?= esc_url(home_url('/')); ?>">
			</a>  
	    </div>
	</div>
	