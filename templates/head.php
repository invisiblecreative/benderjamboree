<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
 <?php wp_head(); ?>
 

<link href='https://fonts.googleapis.com/css?family=Suez+One' rel='stylesheet' type='text/css'>

<link href="https://file.myfontastic.com/n6vo44Re5QaWo8oCKShBs7/icons.css" rel="stylesheet">
 
<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri()?>/assets/scripts/flexslider/flexslider.css" type="text/css"> 
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

<?php  get_template_part('templates/em-parts/em', 'meta'); ?>

<?php the_field('tracking_pixels', $GLOBALS['emid']); ?>


</head>



