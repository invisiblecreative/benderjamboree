<?php include('page-id.php'); ?>

<?php get_template_part('templates/em-parts/em', 'home'); ?>

<?php if( !get_field('em_artist_hide', $page_id)) {
  get_template_part('templates/em-parts/em', 'artists');
} ?>

<?php if( !get_field('em_packages_hide', $page_id) ) {
  get_template_part('templates/em-parts/em', 'package');
} ?>

<?php if( !get_field('em_add_ons_hide', $page_id) ) {
  get_template_part('templates/em-parts/em', 'addons');
} ?>