
<?php
	$eventID = $GLOBALS['emid'];
	if(!is_front_page()){
		$eventID = get_the_ID();
	}
?>

<div class="mdl-grid em-home flex">


		<div class="mdl-cell mdl-cell--6-col share-gallery-row">
			<?php if(get_field('em_section1_lead', $eventID) === 'featurePkg'){
				get_template_part('templates/em-parts/em', 'feature');
			} else { ?>
				<div class="share-card-image mdl-card mdl-shadow--2dp <?php if (get_field('em_section1_lead', $eventID) === 'video'){echo 'embed-container'; }?>">
					<?php if (get_field('em_section1_lead', $eventID) === 'video'){ ?>

							<?php the_field('em_feature_video', $eventID); ?>

					<?php } else { ?>
						<a class="img-popup" href="<?php the_field('em_share_poster', $eventID); ?>">
							<img src="<?php the_field('em_share_poster', $eventID); ?>" />
						</a>
					<?php }?>
				</div>
			<?php } ?>
		</div>

		<div class="home-gallery mdl-cell mdl-cell--6-col share-gallery-row">
			<?php get_template_part('templates/em-parts/em', 'gallery'); ?>
		</div>





</div>
<div class="mdl-grid wt-bkg mdl-shadow--6dp">
	<div class="mdl-cell mdl-cell--8-col">
		<?php the_field('em_welcome_copy', $eventID); ?>
	</div>
	<div class="mdl-cell mdl-cell--4-col">

		<?php get_template_part('templates/em-parts/em', 'milestones'); ?>

		<table class="mdl-data-table mdl-js-data-table full-width topmarg" >
			<thead>
			<tr>
				<th class="mdl-data-table__cell--non-numeric" colspan="4">Stay Connected</th>
			</tr>
			</thead>
			<tbody>
			<tr>
				<?php if( have_rows('social_links', 'option') ): ?>
					<?php while ( have_rows('social_links', 'option') ) : the_row(); ?>
						<td class="mdl-data-table__cell--non-numeric sicon">
							<a class="si socicon-<?php the_sub_field('social_network', 'option'); ?>" href="<?php the_sub_field('social_link_url', 'option'); ?>" target="_blank"></a>
						</td>
					<?php endwhile; ?>
				<?php else : ?>
				<?php endif; ?>
			</tr>
			<tr>
				<th class="mdl-data-table__cell--non-numeric" colspan="4"> Receive Bender updates in your inbox
				</th>
			</tr>
			<tr>
				<td class="mdl-data-table__cell--non-numeric" colspan="4">
					<?php get_template_part('templates/em-parts/em', 'emailjoin'); ?>
				</td>
			</tr>
			</tbody>
		</table>
	</div>
</div>