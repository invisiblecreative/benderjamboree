<?php
	$eventID = $GLOBALS['emid'];
	if(!is_front_page() && !is_page_template('template-lineup.php')){
		$eventID = get_the_ID();
	}
?>
<?php 
if (get_field('em_artist_swap', $eventID ) === false ){
	$artists = get_field('em_line_up', $eventID );
	if( $artists ): ?>
		<div class="mdl-grid em-artists-text <?php if( get_field('em_artist_hide', $eventID ) === true ) {
			echo 'artists-hide';}?>">
			<?php if(get_field('em_artist_headline', $eventID )){ ?>
				<div class="mdl-cell mdl-cell--12-col">
					<h2 class="package-tag"><?php the_field('em_artist_headline', $eventID ); ?></h2>
					<?php if(get_field('em_artist_desc', $eventID )){ ?>
						<p class="package-desc"><?php the_field('em_artist_desc', $eventID ); ?></p>
					<?php } ?>
				</div>
			<?php } ?>
		</div>

		<div class="mdl-grid em-artists <?php if( get_field('em_artist_hide', $eventID) === true	) {
			echo 'artists-hide';}?>">
			<?php foreach( $artists as $post): // variable must be called $post (IMPORTANT) ?>
			<?php setup_postdata($post); ?>

					<div class="mdl-cell mdl-cell--4-col">
					<div class="mdl-card mdl-shadow--6dp">
						<div class="mdl-card__media" style="background-image:url(<?php $img = get_field('artist_photo'); echo $img['url'];?>);">
							<div class="mdl-card__title">
							<h2 class="mdl-card__title-text">
								<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
							</h2>
						</div>
							</div>
					<?php if(get_field('artist_video_url')){ ?>

						<div class="mdl-card__menu">
							<a class="popup-iframe mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect" href="<?php the_field('artist_video_url'); ?>" title="<?php the_title(); ?>">
								<i class="material-icons">play_circle_outline</i>
							</a>
						</div>

					<?php } ?>


					</div>
					</div>

				<?php endforeach; ?>
		</div>
	<?php wp_reset_postdata(); // IMPORTANT - reset the $post object ?>

<?php endif; ?>


<?php } else { ?>
	<div class="mdl-grid em-artists-swap em-home-text <?php if( get_field('em_artist_hide', $eventID) === true) { echo 'artists-hide'; } ?>">
		<div class="mdl-cell mdl-cell--12-col">
			<?php the_field('em_artist_wysiwig', $eventID ); ?>
		</div>
	</div>
<?php } ?>

