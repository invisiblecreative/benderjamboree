<div class="mdl-grid wt-bkg entry-content">
	<div class="mdl-cell mdl-cell--12-col">
		<h2 class="package-tag"><?php the_title(); ?></h2>
		<a name="back-to-top"></a>
	</div>
	<div class="mdl-cell mdl-cell--4-col">
		<table class="mdl-data-table mdl-js-data-table full-width" >
			<thead>
				<tr>
					<th class="mdl-data-table__cell--non-numeric" colspan="">TOPICS:</th>
				</tr>
			</thead>
			<tbody>
			
			<?php if( have_rows('faq_set') ): ?>
			<?php while ( have_rows('faq_set') ) : the_row(); ?>
				
				<tr>
					<td class="mdl-data-table__cell--non-numeric package-price">
						<a href="#<?php the_sub_field('faq_cat_title'); ?>"><?php the_sub_field('faq_cat_title'); ?>
						</a>
					</td>
				</tr>
			
			<?php endwhile; ?>
			<?php else : ?>			
			<?php endif; ?>
				<tr>
					<td class="mdl-data-table__cell--non-numeric package-price">
						  <div class="search-form-wrap">
								<?php get_template_part('templates/searchform'); ?>
							</div>	
					</td>
				</tr>	
			</tbody>
		</table>
		<table class="mdl-data-table mdl-js-data-table full-width topmarg" >
			<thead>
				<tr>
					<th class="mdl-data-table__cell--non-numeric" colspan="">CONTACT SUPPORT</th>		
				</tr>
			</thead>	 
			<tbody>	
				<tr>
					<td class="mdl-data-table__cell--non-numeric" colspan="">
						<div>Our team is here to help you</div>
						<div>Mon - Fri 9:30am - 5:30pm PST</div>
					</td>
				</tr>
				<tr>
					<th class="mdl-data-table__cell--non-numeric" colspan="">Call Us</th>
				</tr>
				<tr>
					<td class="mdl-data-table__cell--non-numeric notop" colspan="">
						<a href="tel:7025283222">702-518-3222</a>
					</td>
				</tr>
				<tr>
					<th class="mdl-data-table__cell--non-numeric" colspan="">START A CHAT SESSION:</th>
				</tr>
				<tr>
					<td class="mdl-data-table__cell--non-numeric notop" colspan="">
					<script type="text/javascript">var $zoho= $zoho || {livedesk:{values:{},ready:function(){}}};var d=document;s=d.createElement("script");s.type="text/javascript";s.defer=true;s.src="https://salesiq.zoho.com/support.bigbluesbender/button.ls?embedname=bigbluesbender";t=d.getElementsByTagName("script")[0];t.parentNode.insertBefore(s,t);document.write("<div id='zldbtnframe'></div>");</script>
					</td>
				</tr>
				<tr>
					<th class="mdl-data-table__cell--non-numeric" colspan="">SEND US AN EMAIL:</th>
				</tr>				
				<tr>		
					<td class="mdl-data-table__cell--non-numeric notop" colspan="">
				
						<script src='https://js.zohostatic.com/support/fbw_v1/zsjquerylib.js'></script><script src='https://js.zohostatic.com/support/fbw_v1/jquery.encoder.min.js'></script><script>var zctt = function(){var tt, mw = 400, top = 10, left = 0, doctt = document;var ieb = doctt.all ? true : false;return{showtt: function(cont, wid){if(tt == null){tt = doctt.createElement('div');tt.setAttribute('id', 'tooltip-zc');doctt.body.appendChild(tt);doctt.onmousemove = this.setpos;doctt.onclick = this.hidett;}tt.style.display = 'block';tt.innerHTML = cont;tt.style.width = wid ? wid + 'px' : 'auto';if(!wid && ieb){tt.style.width = tt.offsetWidth;}if(tt.offsetWidth > mw){tt.style.width = mw + 'px'}h = parseInt(tt.offsetHeight) + top;w = parseInt(tt.offsetWidth) + left;},hidett: function(){tt.style.display = 'none';},setpos: function(e){var u = ieb ? event.clientY + doctt.body.scrollTop : e.pageY;var l = ieb ? event.clientX + doctt.body.scrollLeft : e.pageX;var cw = doctt.body.clientWidth;var ch = doctt.body.clientHeight;if(l < 0){tt.style.left = left + 'px';tt.style.right = '';}else if((l+w+left) > cw){tt.style.left = '';tt.style.right = ((cw-l) + left) + 'px';}else{tt.style.right = '';tt.style.left = (l + left) + 'px';}if(u < 0){tt.style.top = top + 'px';tt.style.bottom = '';}else if((u + h + left) > ch){tt.style.top = '';tt.style.bottom = ((ch - u) + top) + 'px';}else{tt.style.bottom = '';tt.style.top = (u + top) + 'px';}}};}();var zsWebFormMandatoryFields = new Array('Contact Name','Email','Subject');var zsFieldsDisplayLabelArray = new Array('Last Name','Email','Subject');function zsValidateMandatoryFields(){var name = '';var email = '';for(var index = 0; index < zsWebFormMandatoryFields.length; index++){var isError = 0;var fieldObject = document.forms['zsWebToCase_108371000000092377'][zsWebFormMandatoryFields[index]];if(fieldObject){if(((fieldObject.value).replace(/^\s+|\s+$/g, '')).length == 0){alert(zsFieldsDisplayLabelArray[index] +' cannot be empty ');fieldObject.focus();isError = 1;return false;}else{isError = 0;if(fieldObject.name == 'Email'){if(!fieldObject.value.match(/[A-Za-z0-9._%\-+]+@[A-Za-z0-9.\-]+\.[a-zA-Z]{2,22}/)){isError = 1;alert('Enter a valid email-Id');fieldObject.focus();return false;}}}if(fieldObject.nodeName == 'SELECT'){if(fieldObject.options[fieldObject.selectedIndex].value == '-None-'){alert(zsFieldsDisplayLabelArray[index] +' cannot be none');fieldObject.focus();isError = 1;return false;}else{isError = 0;}}if(fieldObject.type == 'checkbox'){if (fieldObject.checked == false){alert('Please accept '+zsFieldsDisplayLabelArray[index]);fieldObject.focus();isError = 1;return false;}else{isError = 0;}}}}}function zsResetWebForm(webFormId){document.forms['zsWebToCase_'+webFormId].reset();} </script>
										
										
										
				<form name='zsWebToCase_108371000000092377' id='zsWebToCase_108371000000092377' action='https://support.zoho.com/support/WebToCase' method='POST' onSubmit='return zsValidateMandatoryFields()' enctype='multipart/form-data'>
				<input type='hidden' name='xnQsjsdp' value='96jn8gajVPJthYo*kRl79w$$'/>
				<input type='hidden' name='xmIwtLD' value='q5B3ZBfnTrowIwVU2bEzCIPapBKEo*Vq'/>
				<input type='hidden' name='actionType' value='Q2FzZXM='/>
				<input type='hidden' name='returnURL' value='*'/>
				
							
						<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
						<input class="mdl-textfield__input" type="text" maxlength='120' name='First Name'>
						<label class="mdl-textfield__label" for="First Name">First Name</label>
						</div>
						<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
						<input class="mdl-textfield__input" type="text" maxlength='120' name='Contact Name'>
						<label class="mdl-textfield__label" for="Contact Name">Last Name</label>
						</div>
						<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
						<input class="mdl-textfield__input" type="text" maxlength='120' name='Email'>
						<label class="mdl-textfield__label" for="Email">Email Address</label>
						</div>
						<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
						<input class="mdl-textfield__input" type="text" maxlength='120' name='Subject'>
						<label class="mdl-textfield__label" for="Subject">Subject</label>
						</div>	
						<div class="mdl-textfield mdl-js-textfield">
						<textarea class="mdl-textfield__input" type="text" rows= "3" name='Description' maxlength='3000' ></textarea>
						<label class="mdl-textfield__label" for="Description">Your Comments...</label>
						</div>
						<div>
						<button type='submit' class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored">
						  SUBMIT
						</button>
						<button class="mdl-button mdl-js-button" value='Reset' onclick="zsResetWebForm('108371000000092377')">
						  RESET
						</button>
						
						</div>
				</form>													
			</td>
		
		
		
		
		
		
		
		</tr>
		 			
			</tbody>
		</table>
				
	</div>
	
	
	<div class="mdl-cell mdl-cell--8-col">			 	
	
<?php if( have_rows('faq_set') ): ?>    
<?php while ( have_rows('faq_set') ) : the_row();	?>
		
			<article>
			<h3><a name="<?php the_sub_field('faq_cat_title'); ?>"></a><?php the_sub_field('faq_cat_title'); ?></h3>	
				
			<ul class="faq-cat-sub-list">
				
<?php while ( have_rows('faq_cat_set') ) : the_row(); ?>
				
				<li class="faq-cat-sub-item">
					<a class="faq-cat-sub-link" href="#<?php the_sub_field( 'faq_question' ); ?> "><?php the_sub_field('faq_question'); ?></a>
				</li>
				
<?php endwhile; ?>
			
			</ul>
		
<?php while ( have_rows('faq_cat_set') ) : the_row(); ?>
			
			<a name="<?php the_sub_field('faq_question'); ?>"></a>
			<section class="faq-group">
				<h6 class="faq-question"><?php the_sub_field('faq_question'); ?></h6>
				<p><?php the_sub_field('faq_answer'); ?></p>
			</section>
			<small><a class="back-to-top" href="#back-to-top">Back to top</a></small>
				
<?php endwhile; ?>	
<?php endwhile; ?>
<?php else : ?>			
<?php endif; ?>
			 			
			
	</div>
	
</div>	
 