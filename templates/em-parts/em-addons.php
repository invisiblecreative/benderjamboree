<?php
	$eventID = $GLOBALS['emid'];
	if(!is_front_page() && !is_page_template('template-packages.php')){
		$eventID = get_the_ID();
	}
?>
<?php
if(is_singular( 'packages_post_type' )){
	$addons = get_field('pkg_available_add_ons', $package_id );
}else{	
	$addons = get_field('em_add_ons', $eventID );
}
$package_id = get_the_ID();

if( $addons ): ?>

<div class="mdl-grid em-addons <?php if( get_field('em_add_ons_hide', $eventID ) === true) { echo
'addon-hide';}?>">
 	<?php if(get_field('em_addon_headline', $eventID )){ ?>
	<div class="mdl-cell mdl-cell--12-col em-home-text">
		<h2 class="addon-tag"><?php the_field('em_addon_headline', $eventID ); ?></h2>
	</div>
	<?php } ?>
	<?php foreach( $addons as $post): // variable must be called $post (IMPORTANT) ?>
	<?php setup_postdata($post); ?>        
            
    <div class="mdl-cell mdl-cell--4-col addons-grid">
		
		<div class="mdl-card mdl-shadow--6dp">
			<a class="addon-link mdl-card__media" href="<?php the_permalink(); ?>" style="background-image:url(<?php the_field('pkg_add_on_graphic'); ?>);">
					<div class="mdl-card__title">
						<h2 class="mdl-card__title-text">
							<?php the_title(); ?>
						</h2>
					</div>
	  		
			</a>
			
			
			<div class="mdl-grid mdl-card__actions mdl-card--border">
				
				
				<div class="addon_price mdl-card__supporting-text">
					<?php if(is_singular( 'packages_post_type' )){?>
						<?php $addon_name = $post->post_name; ?>	
						
						<?php if($addon_name == "extender"){?>								
							$<?php the_field('pkg_add_a_night_price', $package_id );?> <span>per night</span>
						<?php } else { ?>								
							<?php the_field('add_on_home_price_detail'); ?>
						<?php } ?>
				<?php } else { ?>
						<?php the_field('add_on_home_price_detail'); ?>
				<?php } ?>

				</div>
				
				 
				
			</div>
			<div class="mdl-card__menu">
				<a class="mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect" href="<?php the_permalink(); ?>">
					<i class="material-icons">info</i> 
				</a>
			</div>
			
			
			
				
  						
		</div>
    </div>
               
    <?php endforeach; ?>
</div>
<?php wp_reset_postdata(); // IMPORTANT - reset the $post object ?>
    
<?php endif; ?>



 