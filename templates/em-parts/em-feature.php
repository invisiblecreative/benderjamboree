<?php
  $eventID = $GLOBALS['emid'];
  if(!is_front_page()){
    $eventID = get_the_ID();
  }
?>
<?php
$feature = get_field('em_feature_object', $eventID );
if( $feature ): ?>

<div class="em-packages">

  <?php foreach( $feature as $post): // variable must be called $post (IMPORTANT) ?>
    <?php setup_postdata($post); ?>

    <div class="<?php the_field('pkg_sales_status');?>">
      <div class="mdl-card mdl-shadow--6dp wt-bkg">
        <a class="pkg-link mdl-card__media" href="<?php the_permalink(); ?>" style="background-image:url(<?php $img = get_field('pkg_image'); echo $img['url']; ?>);">
          <div class="mdl-card__title">
            <h2 class="mdl-card__title-text">
              <?php the_title(); ?>
            </h2>
          </div>

        </a>
<!--
        <div class="mdl-card--border mdl-card__supporting-text home-feature">
            <?php /* if(get_field('pkg_tag_line')){ ?>
              <h4 class="package-tagline"><?php the_field('pkg_tag_line'); ?></h4>
            <?php } */ ?>
            <?php /* if(get_field('pkg_description')){ ?>
              <p class="package-desc"><?php the_field('pkg_description'); ?></p>
            <?php } */?>
        </div>
-->
        <div class="mdl-grid mdl-card__actions mdl-card--border">


          <div class="pkg_price mdl-card__supporting-text">
            <?php get_template_part('templates/packages/package', 'ppprice'); ?>
          </div>

          <div class="sales_btn_wrap mdl-card__actions">
            <?php get_template_part('templates/packages/package', 'salesbtn'); ?>

          </div>

        </div>
        <div class="mdl-card__menu">
          <a class="mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect" href="<?php the_permalink(); ?>">
            <i class="material-icons">info</i>
          </a>
        </div>
      </div>
    </div>

  <?php endforeach; ?>
</div>
<?php wp_reset_postdata(); // IMPORTANT - reset the $post object ?>
<?php endif; ?>