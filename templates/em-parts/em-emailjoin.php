<?php
	$eventID = $GLOBALS['emid'];
	if(!is_front_page()){
		$eventID = get_the_ID();
	}
?>
<div id="mc_embed_signup">
	<?php if( have_rows('em_mailchimp_params', $eventID) ): ?>
	<?php while ( have_rows('em_mailchimp_params', $eventID) ) : the_row(); ?>
	<form action="//<?php the_sub_field('em_mc_subscribe_url', $eventID); ?>/subscribe/post?u=<?php the_sub_field('em_mc_user_id', $eventID); ?>&amp;id=<?php the_sub_field('em_mc_list_id', $eventID); ?>" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate email-join" novalidate>	
	<?php endwhile; ?><?php else : ?><?php endif; ?>	
		<div id="mc_embed_signup_scroll">
			<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label mc-field-group" >
				<input type="email" value="" name="EMAIL" class="mdl-textfield__input required email" id="mce-EMAIL">
				<label class="mdl-textfield__label" for="sample1">Your email address</label>
				
				<button type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored">
				JOIN
				</button>
			</div>
		
			<div id="mce-responses" class="clear">
			<div class="response" id="mce-error-response" style="display:none;clear:both;"></div>
			<div class="response" id="mce-success-response" style="display:none"></div>
			</div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
			<div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_655843c8741105f48fc0939d7_01c682b040" tabindex="-1" value=""></div>
		
		</div>	
	</form>
</div>

<script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';}(jQuery));var $mcj = jQuery.noConflict(true);</script>	