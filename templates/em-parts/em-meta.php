<?php
	$eventID = $GLOBALS['emid'];
	/* TODO WRITE CONDTIONS TO CHANGE THE DATA TO REFLECT REWInd DATA ON REWIND PAGES
	if(!is_front_page()){
		$eventID = get_the_ID();
	}
	*/
?>

<?php 	
	$canonical = get_bloginfo('wpurl');
	$title = get_bloginfo('name');
	$tag = get_bloginfo('description'); 	
	 
	$thispage = get_the_title();
	$thisurl = get_page_link();
	
	$year = date("Y");
	$startdate = get_field('em_start_date', $eventID); 
	$start = date("F j-", strtotime($startdate));
	$enddate = get_field('em_end_date', $eventID); 
	$end = date("j, Y", strtotime($enddate));
	 	
	$venue = get_field('em_venue_name', $eventID);
	
	$owner = get_field('em_promoter', $eventID); 
	
	$fbadmin = get_field('em_facebook_page_admin_id', $eventID);
	$img = get_field('fb_og_share', $eventID);
	$fbshare = $img['url'];
	
	

?>

<link rel="canonical" href="<?php echo $canonical;?>" />
<meta property="og:site_name" content="<?php echo $title; ?>" />

<meta name="viewport" content="width=device-width" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<meta name="distribution" content="GLOBAL">
<meta name="rating" content="GENERAL">
<meta property="og:type" content="website" />
<!-- 
	<meta name="author" content="invisiblecreative.com | sweetwoodcreative.com">
-->

<meta property="og:title" content="<?php echo $title .' '. $thispage; ?>" />
<meta property="og:url" content="<?php echo $thisurl; ?>" />

<meta name="description" property="og:description" content="<?php echo $thispage .' | '. $title .' - '. $start . $end .' - '. $venue; ?>">

<meta name="copyright" content="Copyright © <?php echo $year .', '. $owner; ?>">
<meta name="owner" content="<?php echo $owner; ?>">

<meta property="og:image" content="<?php echo $fbshare; ?>" />
<meta property="og:image:type" content="image/jpeg" />
<meta property="og:image:width" content="1200" />
<meta property="og:image:height" content="627" />

<meta property="fb:admins" content="<?php echo $fbadmin; ?>" />