<?php
	$eventID = $GLOBALS['emid'];
	if(!is_front_page()){
		$eventID = get_the_ID();
	}
?>
 <?php
	 $today = current_time( 'm/d/Y', $gmt = 0 ); 
	 $mscount = get_field('milestones_to_show', $eventID);
	?>

<?php if( have_rows('em_milestones', $eventID) && !get_field('milestones_hide', $eventID)):  $i = 0; ?>
			    
			    
			    <table class="mdl-data-table mdl-js-data-table  full-width" >
					<thead>
					<tr>
					<th class="mdl-data-table__cell--non-numeric" colspan="2">Upcoming Milestones: <i class="material-icons alllink"><a href="milestones">view_list</a></i></th>

					</tr>
					</thead>
					<tbody>
			    
					<?php while ( have_rows('em_milestones', $eventID) ) : the_row();
							$date1=date_create($today);
							$date2=date_create(get_sub_field('em_milestone_date', $eventID));
							$diff=date_diff($date1,$date2);
							$ff = $diff->format("%R%a"); ?>
					
					
					<?php
					if ($ff >= 0 ) :  $i++; if ($i <= $mscount ) :  ?>
					
							
					<tr>
					<td class="mdl-data-table__cell--non-numeric"><?php the_sub_field('em_milestone_date', $eventID); ?></td>
					
					<td class="mdl-data-table__cell--non-numeric "><?php the_sub_field('em_milestone', $eventID) ?></td>
					</tr>
					
					<?php endif; ?>
					<?php endif; ?>
	
					<?php endwhile; ?>
			    
			     
			    
			    </tbody>
				</table>
				<?php else : ?>
			
				
			
			<?php endif; ?>
			
		 