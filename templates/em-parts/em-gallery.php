<?php

	$eventID = $GLOBALS['emid'];
	if(!is_front_page()){
		$eventID = get_the_ID();
	}


	$images = '';
if(get_field('em_section1_gallery', $eventID) == 'facebookGal')	{
	$images = get_field('em_facebook_gallery', $eventID);
}elseif(get_field('em_section1_gallery', $eventID) == 'custom'){
	$images = get_field('em_gallery', $eventID);
}
if( $images ): ?>

    <div id="slider_<?php echo $eventID;?>" class="flexslider slider">
        <ul class="slides">
        <?php foreach( $images as $image ):
					$artist_slide = get_field('gallery_artist_post', $image['ID']);
					$images = '';
					if(get_field('em_section1_gallery', $eventID) == 'facebookGal')	{
						$imageURL = $image['src'];
					} elseif ( get_field('em_section1_gallery', $eventID) == 'custom' ){
						$imageURL = $image['url'];
					}
					?>
            <li>
            	<a href="<?php
									if( get_field('gallery_link_type', $image['ID']) == "gallery-iframe" ){
										the_field('gallery_link_url', $image['ID']);
	            		} elseif ( get_field('gallery_link_type', $image['ID']) == "artist-slide" ){
										// override $post
										$post = $artist_slide;
										setup_postdata( $post );
										the_permalink($post);
										wp_reset_postdata();
									} else {
										echo $imageURL;
									}?>" <?php
									if (get_field('gallery_link_type', $image['ID']) == "gallery-iframe" ){ ?>
										class="gallery-iframe"
						<?php } elseif (get_field('gallery_link_type', $image['ID']) == "artist-slide"){ ?>

							<?php } else { ?>
									class="flex-pop"
							<?php }
									if(get_field('gallery_link_type', $image['ID']) == "_blank"){?>
									target="_blank"
									<?php } ?>
								style="background-image: url(<?php echo $imageURL; ?>);">
								<?php if(get_field('gallery_link_type', $image['ID']) == "artist-slide"){?>
							<h3 class="artist-slide-title"><?php
									$artist_slide = get_field('gallery_artist_post', $image['ID']);
									if( $artist_slide ):
										// override $post
										$post = $artist_slide;
										setup_postdata( $post );
										the_title();
										wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly
									endif; ?>
							</h3>
						<?php }?>
						</a>
					</li>
        <?php endforeach; ?>
        </ul>
    </div>
<?php endif; ?>
 