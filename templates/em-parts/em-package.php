<?php
	$eventID = $GLOBALS['emid'];
	if(!is_front_page() && !is_page_template('template-packages.php')){
		$eventID = get_the_ID();
	}
?>
<?php
	if (get_field('em_packages_swap', $eventID ) === false ){

		$packages = get_field('em_packages', $eventID );
		if( $packages ): ?>

			<div class="mdl-grid em-home-text <?php if( get_field('em_packages_hide', $eventID) === true
			) { echo 'package-hide';}?>">
				<div class="mdl-cell mdl-cell--12-col">
					<?php if(get_field('em_packages_headline', $eventID )){ ?>
						<h2 class="package-tag"><?php the_field('em_packages_headline', $eventID ); ?></h2>
					<?php } ?>
					<?php if(get_field('em_packages_paragraph', $eventID )){ ?>
						<p class="package-desc"><?php the_field('em_packages_paragraph', $eventID ); ?></p>
					<?php } ?>
				</div>
			</div>

			<div class="mdl-grid em-packages <?php if( get_field('em_packages_hide', $eventID) === true
				) { echo 'package-hide';}?>">

				<?php foreach( $packages as $post): // variable must be called $post (IMPORTANT) ?>
				<?php setup_postdata($post); ?>

					<div class="mdl-cell mdl-cell--4-col  <?php the_field('pkg_sales_status');?>">
					<div class="mdl-card mdl-shadow--6dp">
						<a class="pkg-link mdl-card__media" href="<?php the_permalink(); ?>" style="background-image:url(<?php $img = get_field('pkg_image'); echo $img['url']; ?>);">
								<div class="mdl-card__title">
									<h2 class="mdl-card__title-text">
										<?php the_title(); ?>
									</h2>
								</div>

						</a>

						<div class="mdl-grid mdl-card--border mdl-card__supporting-text">


							<div class="pkg_options ">
								<?php get_template_part('templates/packages/package', 'icons'); ?>
							</div>



						</div>
						<div class="mdl-grid mdl-card__actions mdl-card--border">


							<div class="pkg_price mdl-card__supporting-text">
								<?php get_template_part('templates/packages/package', 'ppprice'); ?>
							</div>

							<div class="sales_btn_wrap mdl-card__actions">
								<?php get_template_part('templates/packages/package', 'salesbtn'); ?>

							</div>

						</div>
						<div class="mdl-card__menu">
							<a class="mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect" href="<?php the_permalink(); ?>">
								<i class="material-icons">info</i>
							</a>
						</div>
					</div>
					</div>

					<?php endforeach; ?>
			</div>
			<?php wp_reset_postdata(); // IMPORTANT - reset the $post object ?>
		<?php endif; ?>




<?php } else { ?>
	<div class="mdl-grid em-packages-swap em-home-text <?php if( get_field('em_packages_hide',
					$eventID) === true ) { echo 'package-hide';}?>">
		<div class="mdl-cell mdl-cell--12-col">
			<?php the_field('em_packages_wysiwyg', $eventID ); ?>
		</div>
	</div>
<?php } ?>