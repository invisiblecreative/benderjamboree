
<?php	
	$field_location = get_field_object('pkg_location');
	$value_location = get_field('pkg_location');
	$label_location = $field_location['choices'][ $value_location ];
	
	$field_bedding = get_field_object('bedding_preference');
	$value_bedding = get_field('bedding_preference');
	$label_bedding = $field_bedding['choices'][ $value_bedding ];
	
	$field_smoking = get_field_object('pkg_smoking_preference');
	$value_smoking = get_field('pkg_smoking_preference');
	$label_smoking = $field_smoking['choices'][ $value_smoking ];
	
	$field_occupancy = get_field_object('pkg_occupancy');
	$value_occupancy = get_field('pkg_occupancy');
	$label_occupancy = $field_occupancy['choices'][ $value_occupancy ];
	?>
	
	



<table class="mdl-data-table mdl-js-data-table full-width" >
  <thead>
    <tr>
      <th class="mdl-data-table__cell--non-numeric" colspan="3">Package Details</th>
       
    </tr>
  </thead>
  <tbody>
	<tr>
      <td class="mdl-data-table__cell--non-numeric min-w" ><i class="material-icons">business</i></td>
      <td class="mdl-data-table__cell--non-numeric">Location:</td>
      <td class="mdl-data-table__cell--non-numeric"> <?php echo $label_location; ?></td>
    </tr>
    
    	<tr>
      <td class="mdl-data-table__cell--non-numeric min-w" ><i class="material-icons">local_hotel</i></td>
      <td class="mdl-data-table__cell--non-numeric">Bedding Choice:</td>
      <td class="mdl-data-table__cell--non-numeric"> <?php echo $label_bedding; ?></td>
    </tr>
    
    	<tr>
      <td class="mdl-data-table__cell--non-numeric min-w" ><i class="material-icons">smoking_rooms</td>
      <td class="mdl-data-table__cell--non-numeric">Smoking Choice:</td>
      <td class="mdl-data-table__cell--non-numeric"> <?php echo $label_smoking; ?></td>
    </tr>
    
    	<tr>
      <td class="mdl-data-table__cell--non-numeric min-w" ><i class="material-icons">group</i></td>
      <td class="mdl-data-table__cell--non-numeric">Occupancy:</td>
      <td class="mdl-data-table__cell--non-numeric"> <?php echo $label_occupancy; ?></td>
    </tr>


  </tbody>
</table>


  