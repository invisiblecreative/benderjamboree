
<!----- this whole document is now #zombiecode


<?php $package_id = get_the_ID(); ?>
	
<?php $pkg_addons = get_field('pkg_available_add_ons');
		if( $pkg_addons ): ?>
		
		    <?php foreach( $pkg_addons as $post): // variable must be called $post (IMPORTANT) ?>
		        <?php setup_postdata($post); ?>
		        
		        
		        <div class="mdl-cell mdl-cell--3-col addons-grid">
		
					<div class="mdl-card mdl-shadow--6dp">
						<a class="addon-link mdl-card__media" href="<?php the_permalink(); ?>" style="background-image:url(<?php the_field('pkg_add_on_graphic'); ?>);">
							<div class="mdl-card__title">
									<h2 class="mdl-card__title-text">
										<?php the_title(); ?>
									</h2>
							</div>	
				  		
						</a>
						
						
						<div class="mdl-grid mdl-card__actions mdl-card--border">
							
							
							<div class="addon_price mdl-card__supporting-text">
							<?php $addon_name = $post->post_name; ?>	
								
							<?php if($addon_name == "extender"){?>								
								$<?php the_field('pkg_add_a_night_price', $package_id );?> <span>per night</span>
							<?php } else { ?>								
								<?php the_field('add_on_home_price_detail'); ?>
							<?php } ?>
							</div>
							
							 
							
						</div>
						<div class="mdl-card__menu">
							<i class="material-icons">info</i>
						</div>
							
			  						
					</div>
			    </div>
			               
			    <?php endforeach; ?>
			

		    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
		<?php endif; ?>
		
--------->
