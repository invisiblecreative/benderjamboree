<?php
$eventID = $GLOBALS['emid'];
if(!is_front_page() && !is_page_template('template-packages.php') && !is_singular()){
$eventID = get_the_ID();
}
//EVENT DETAIL FIELD VALUES AND LABLES
$field_site_sales = get_field_object('em_sales_status', $eventID);
$value_site_sales = get_field('em_sales_status', $eventID);
$label_site_sales = $field_site_sales['choices'][ $value_site_sales ];
$site_book_status = $value_site_sales;
?>
<table class="mdl-data-table mdl-js-data-table full-width topmarg" >
	<thead>
		<tr>
			<th class="mdl-data-table__cell--non-numeric" colspan="">Pricing Breakdown:</th>
		
		</tr>
	</thead>
	<tbody>
		<tr>
			<td class="mdl-data-table__cell--non-numeric package-price">$<?php the_field('pkg_price_pp'); ?> <?php if($site_book_status == 'prebook_on_sale') {?>per room<?php } else {?>per person<?php }?></td>
		</tr>
		
		<tr>
			<td class="mdl-data-table__cell--non-numeric" >Based on <?php	
			$field_occupancy = get_field_object('pkg_occupancy');
			$value_occupancy = get_field('pkg_occupancy');
			$label_occupancy = $field_occupancy['choices'][ $value_occupancy ];
			echo $label_occupancy;
			?></td>
		</tr>
		<tr>
			<td class="mdl-data-table__cell--non-numeric pkg-pr-price">$<?php the_field('pkg_price_pr'); ?> Total Package Price</td> 
		</tr>
		
		<tr>
			<td class="mdl-data-table__cell--non-numeric let">Nevada Live Entertainment Tax is required by law,<br /> and will appear at checkout.</td> 
		</tr>  
	
	</tbody>
</table>



