<div class="mdl-cell mdl-cell--12-col">
			<h2 class="package-tag"><?php the_title(); ?></h2>
		<?php if(get_field('pkg_tag_line')){ ?>
			<h4 class="package-tagline"><?php the_field('pkg_tag_line'); ?></h4>
		<?php } ?>
		<?php if(get_field('pkg_description')){ ?>
			<p class="package-desc"><?php the_field('pkg_description'); ?></p>
		<?php } ?>
</div>
