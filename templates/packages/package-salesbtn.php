<?php
$eventID = $GLOBALS['emid'];
if(!is_front_page() && !is_page_template('template-packages.php') && !is_singular()){
	$eventID = get_the_ID();
}
//EVENT DETAIL FIELD VALUES AND LABLES
$field_site_sales = get_field_object('em_sales_status', $eventID);
$value_site_sales = get_field('em_sales_status', $eventID);
$label_site_sales = $field_site_sales['choices'][ $value_site_sales ];
$site_book_status = $value_site_sales;
//PACKAGE FIELD VALUES AND LABLES
$field_package_sales = get_field_object('pkg_sales_status');
$value_package_sales = get_field('pkg_sales_status');
$label_package_sales = $field_package_sales['choices'][ $value_package_sales ];
$package_book_status = $value_package_sales;
// Setting up base booking url
$pkg_book_url = get_field('pkg_book_url');
$book_url = get_field('em_booking_url', $eventID);
// SETTING UP THE TRIP ID REZMAGIC
if($site_book_status == 'prebook_on_sale') {
	$tripID = get_field('em_prebooking_trip_id', $eventID);
} else {
	$tripID = get_field('em_rezmagic_trip_id', $eventID);
}
// BOOK BUTTON URL BUILD
$trip = "";
if($pkg_book_url != "") {
	$trip = $pkg_book_url;
} else{
	$trip = $book_url . '/Booking/Reservation/Start?tripID=' . $tripID;
}
// BOOK BUTTON LINK
if ($site_book_status == 'prebook_now' || $site_book_status == 'on_sale' || $site_book_status == 'prebook_on_sale' ){
	if ($package_book_status == 'sold_out' ){
		$book_link = '#';
	} else {
		$book_link = $trip;
	}
} else {
	$book_link = '#';
}
// BOOK BUTTON TEXT
$sitewide_sales_text = get_field('em_sitewide_sales_text', $eventID);
$book_btn_text = "";
if ( $site_book_status == 'prebook_now' ||
		 $site_book_status == 'on_sale' ||
		 $site_book_status == 'sold_out' ||
		 $site_book_status == 'closed' ||
		 $site_book_status == 'prebook_on_sale' ){
	if (
      $site_book_status == 'prebook_now' ||
      $site_book_status == 'on_sale' ||
      $site_book_status == 'prebook_on_sale'
  ){
    if(
      $package_book_status == 'sold_out' ||
      $package_book_status == 'limited'
    ){
      $book_btn_text = $label_package_sales;
    } else {
      $book_btn_text = $label_site_sales;
    }
	} else {
		$book_btn_text = $label_site_sales;
	}
} elseif (
		$site_book_status == 'prebook_soon' ||
		$site_book_status == 'prebook_announce'
	){
		$book_btn_text = $sitewide_sales_text;
} ?>
<div class="book-btn-container book-btn-container-<?php echo $site_book_status;?>  book-btn-container-<?php echo $package_book_status;?> mdl-shadow--2dp">
	<a
		class="book-btn <?php echo $site_book_status; ?> book-btn-<?php echo $package_book_status;?>"
		href="<?php echo $book_link; ?>" target="_blank" ><?php echo $book_btn_text; ?></a>
</div>