
	<?php	
	$field_location = get_field_object('pkg_location');
	$value_location = get_field('pkg_location');
	$label_location = $field_location['choices'][ $value_location ];
	?>	
	<div class="pkg_icons">
		<span><i class="material-icons">business</i> <?php echo $label_location; ?></span>
	</div>
	<?php	
	$field_bedding = get_field_object('bedding_preference');
	$value_bedding = get_field('bedding_preference');
	$label_bedding = $field_bedding['choices'][ $value_bedding ];
	?>
	<div class="pkg_icons">	
		<span><i class="material-icons">local_hotel</i> <?php echo $label_bedding; ?></span>
	</div>
	<?php	
	$field_smoking = get_field_object('pkg_smoking_preference');
	$value_smoking = get_field('pkg_smoking_preference');
	$label_smoking = $field_smoking['choices'][ $value_smoking ];
	?>
	<div class="pkg_icons">
		<span><i class="material-icons">smoking_rooms</i> <?php echo $label_smoking; ?></span>
	</div>
	<?php	
	$field_occupancy = get_field_object('pkg_occupancy');
	$value_occupancy = get_field('pkg_occupancy');
	$label_occupancy = $field_occupancy['choices'][ $value_occupancy ];
	?>	
	<div class="pkg_icons">
		<span><i class="material-icons">group</i> <?php echo $label_occupancy; ?></span>
	</div>

