<?php
  $eventID = $GLOBALS['emid'];
  if(!is_front_page() && !is_page_template('template-packages.php')){
    $eventID = get_the_ID();
  }
  //EVENT DETAIL FIELD VALUES AND LABLES
  $field_site_sales = get_field_object('em_sales_status', $eventID);
  $value_site_sales = get_field('em_sales_status', $eventID);
  $label_site_sales = $field_site_sales['choices'][ $value_site_sales ];
  $site_book_status = $value_site_sales;
  ?>
	<span class="package-price">$<?php the_field('pkg_price_pp'); ?> <span><?php if($site_book_status == 'prebook_on_sale') {?>per room<?php } else {?>per person<?php }?></span></span>

