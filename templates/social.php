  <?php if( have_rows('social_links', 'option') ): ?>
					<?php while ( have_rows('social_links', 'option') ) : the_row(); ?>
					
					<div class="sicon">
					<a class="si socicon-<?php the_sub_field('social_network', 'option'); ?>" href="<?php the_sub_field('social_link_url', 'option'); ?>" target="_blank"></a></div>
					
					<?php endwhile; ?>
					<?php else : ?>
					<?php endif; ?>