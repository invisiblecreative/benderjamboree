<div class="mdl-grid wt-bkg entry-content sing-addon <?php // the_field('pkg_sales_status');  #zombiecode? ?>">
  <div class="mdl-cell mdl-cell--7-col">

    <div class="mdl-cell mdl-cell--12-col">
      <h2 class="package-tag"><?php the_title(); ?></h2>
      <?php if(get_field('pkg_add_on_short_desc')) { ?>
        <h4 class="package-tagline"><?php the_field('pkg_add_on_short_desc'); ?></h4>
      <?php } ?>
      <?php if(get_field('pkg_add_on_long_desc')) { ?>
        <p class="package-desc"><?php the_field('pkg_add_on_long_desc'); ?></p>
      <?php } ?>
    </div>


    <?php if(have_rows('add_on_includes')): ?>
      <span class="pkg_sub_head">Package Includes:</span>

      <ul class="pkg_includes">

        <?php while(have_rows('add_on_includes')) : the_row(); ?>

          <li><?php the_sub_field('add_on_include'); ?></li>

        <?php endwhile; ?>
      </ul>
    <?php else : ?>

    <?php endif; ?>
    <?php if(have_rows('add_on_notes')): ?>

      <span class="pkg_sub_head">Please note:</span>

      <ul class="pkg_notes">
        <?php while(have_rows('add_on_notes')) : the_row(); ?>

          <li><?php the_sub_field('add_on_note'); ?></li>

        <?php endwhile; ?>
      </ul>
    <?php else : ?>

    <?php endif; ?>


    <?php if(have_rows('add_on_pricing_table')): ?>

      <table class="mdl-data-table mdl-js-data-table full-width topmarg">
        <thead>
        <tr>
          <th class="mdl-data-table__cell--non-numeric" colspan="2">Pricing Breakdown:</th>

        </tr>
        </thead>
        <tbody>

        <?php while(have_rows('add_on_pricing_table')) : the_row(); ?>

          <?php $values = get_sub_field('add_on_table_price');
          if($values) {
            foreach($values as $value): ?>
              <tr>
                <td class="mdl-data-table__cell--non-numeric package-price">$<?php the_field('pkg_add_a_night_price', $value); ?> per night</td>
                <td class="mdl-data-table__cell--non-numeric "><?php echo get_the_title($value); ?></td>
              </tr>
              <?php
            endforeach;
          } ?>
        <?php endwhile; ?>
        <tr>
          <td class="mdl-data-table__cell--non-numeric let " colspan="2">Prices include 13% Hotel tax & $18 Hotel Daily Resort fee</td>
        </tr>
        </tbody>
      </table>


    <?php endif; ?>


  </div>
  <div class="mdl-cell mdl-cell--5-col ">
    <img src="<?php the_field('pkg_add_on_graphic'); ?>" width="100%"/>
    <?php if(get_field('add_on_disclaimer')): ?>
      <table class="mdl-data-table mdl-js-data-table full-width">
        <thead>
          <tr>
            <th class="mdl-data-table__cell--non-numeric" colspan="3">Before you Buy</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td class="mdl-data-table__cell--non-numeric disclaim"> <?php the_field('add_on_disclaimer'); ?></td>
          </tr>
        </tbody>
      <?php endif; ?>
      <?php if(get_field('add_on_opt_1_name')): ?>
        <table class="mdl-data-table mdl-js-data-table full-width topmarg">
          <thead>
            <tr>
              <th class="mdl-data-table__cell--non-numeric" colspan="2">Pricing Breakdown:</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td class="mdl-data-table__cell--non-numeric package-price">$<?php the_field('add_on_opt_1_price'); ?> per person</td>
              <td class="mdl-data-table__cell--non-numeric "><?php the_field('add_on_opt_1_name'); ?></td>
            </tr>
      <?php if(get_field('add_on_opt_2_name')): ?>

            <tr>
              <td class="mdl-data-table__cell--non-numeric package-price">$<?php the_field('add_on_opt_2_price'); ?> per person</td>
              <td class="mdl-data-table__cell--non-numeric "><?php the_field('add_on_opt_2_name'); ?></td>
            </tr>

          <?php endif; ?>

          <tr>
            <td class="mdl-data-table__cell--non-numeric let" colspan="2"><?php the_field('add_on_fee'); ?></td>
          </tr>
          </tbody>
        </table>


      <?php endif; ?>

      <?php
        $eventID = $GLOBALS['emid'];
        if(!is_front_page() && !is_page_template('template-packages.php') && !is_singular()){
          $eventID = get_the_ID();
        }
        //EVENT DETAIL FIELD VALUES AND LABLES
        $field_site_sales = get_field_object('em_sales_status', $eventID);
        $value_site_sales = get_field('em_sales_status', $eventID);
        $label_site_sales = $field_site_sales['choices'][ $value_site_sales ];
        $site_book_status = $value_site_sales;

        //PACKAGE FIELD VALUES AND LABLES
        $field_package_sales = get_field_object('pkg_sales_status');
        $value_package_sales = get_field('pkg_sales_status');
        $label_package_sales = $field_package_sales['choices'][ $value_package_sales ];
        $package_book_status = $value_package_sales;

        $add_on_avail = get_field('add_on_availibility');
        if($add_on_avail == 1) {
          $avail_label = 'No Longer Available';
        } else {
          $avail_label = 'Add to your Reservation';
        }

        // Setting up base booking url
        $add_on_book_url = get_field('add_on_url');
        $pkg_book_url = get_field('pkg_book_url');
        $book_url = get_field('em_booking_url', $eventID);
        // SETTING UP THE TRIP ID REZMAGIC
        if($site_book_status == 'prebook_on_sale') {
          $tripID = get_field('em_prebooking_trip_id', $eventID);
        } else {
          $tripID = get_field('em_rezmagic_trip_id', $eventID);
        }
        // BOOK BUTTON URL BUILD
        $trip = "";
        if($add_on_book_url != "") {
          $trip = $add_on_book_url;
        } else{
          $trip = $book_url . '/Booking/Reservation/Start?tripID=' . $tripID;
        }
        // BOOK BUTTON LINK
        if ($site_book_status == 'prebook_now' || $site_book_status == 'on_sale' || $site_book_status == 'prebook_on_sale' ){
          if ($package_book_status == 'sold_out' ){
            $book_link = '#';
          } else {
            $book_link = $trip;
          }
        } else {
          $book_link = '#';
        }
        // BOOK BUTTON TEXT
        $sitewide_sales_text = get_field('em_sitewide_sales_text', $eventID);
        $book_btn_text = "";
        if ( $site_book_status == 'prebook_now' ||
                $site_book_status == 'on_sale' ||
                $site_book_status == 'sold_out' ||
                $site_book_status == 'closed' ||
                $site_book_status == 'prebook_on_sale' ){
          if (
                  $site_book_status == 'prebook_now' ||
                  $site_book_status == 'on_sale' ||
                  $site_book_status == 'prebook_on_sale'
          ){
            if(
                    $add_on_avail == 1
            ){
              $book_btn_text = $avail_label;
            } else {
              $book_btn_text = $label_site_sales;
            }
          } else {
            $book_btn_text = $label_site_sales;
          }
        } elseif (
                $site_book_status == 'prebook_soon' ||
                $site_book_status == 'prebook_announce'
        ){
          $book_btn_text = $sitewide_sales_text;
        } ?>
        <div class="book-btn-container book-btn-container-<?php echo $site_book_status;?>  book-btn-container-<?php if($add_on_avail == 1){?>sold_out<?php }else{?>default<?php }?> mdl-shadow--2dp">
          <a
                  class="book-btn <?php echo $site_book_status; ?> book-btn-<?php if($add_on_avail == 1){?>sold_out<?php }else{?>default<?php }?>"
                  href="<?php echo $book_link; ?>" target="_blank" ><?php echo $book_btn_text; ?></a>
        </div>

  </div>
</div>

<div class="mdl-grid">
  <?php // get_template_part('templates/packages/package', 'addons'); ?>
</div>