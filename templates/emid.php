<?php
global $emid;  
//Query the most recent published post in Event Detail - Event Status
	$args = array( 'numberposts' => '1', 'order' => 'DESC','post_type' => 'event_detail','event_status' => 'current','post_status' => 'publish' );
	$recent_posts = wp_get_recent_posts( $args );
	foreach( $recent_posts as $recent ){
    	$emid = $recent["ID"];    
    }
?>