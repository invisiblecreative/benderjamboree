<div class="mdl-grid wt-bkg entry-content <?php the_field('pkg_sales_status');?>">
	<div class="mdl-cell mdl-cell--7-col">
		
			<?php get_template_part('templates/packages/package', 'text'); ?>
			
			<?php if( have_rows('pkg_includes') ): ?>
			   <span class="pkg_sub_head">Package Includes:</span>
			   
			    <ul class="pkg_includes" >
				    
					<?php while ( have_rows('pkg_includes') ) : the_row(); ?>
					
					<li><?php the_sub_field('pkc_include'); ?></li>
					
					<?php endwhile; ?>
			    </ul>
				<?php else : ?>
			
			<?php endif; ?>
			<?php if( have_rows('pkg_notices') ): ?>
			    
			    <span class="pkg_sub_head">Please note:</span>
			   
			    <ul class="pkg_notes" >
					<?php while ( have_rows('pkg_notices') ) : the_row(); ?>
					
					<li><?php the_sub_field('pkg_notice'); ?></li>
					
					<?php endwhile; ?>
			    </ul>
				<?php else : ?>
			
			<?php endif; ?>
			
			
			<?php if(get_field('pkg_additional_person')){ ?>
			 
			
			<span class="pkg_sub_head">Additional Guests:</span>	
	
	<ul class="pricing_table">
	<li class="pkg-add-per"><?php the_field('pkg_additional_person'); ?> </li>
	</ul>
		<?php } ?>
	</div>
	<div class="mdl-cell mdl-cell--5-col">
		<img src="<?php $img = get_field('pkg_image'); echo $img['url']; ?>" width="100%"/>
		<?php get_template_part('templates/packages/package', 'singleicons'); ?>
		
		<?php get_template_part('templates/packages/package', 'pricing'); ?> 
		
		<?php get_template_part('templates/packages/package', 'salesbtn'); ?> 
		
	</div>
</div>	

<?php get_template_part('templates/em-parts/em', 'addons'); ?>
