<?php if ( is_active_sidebar( 'sidebar-mega-footer' ) ) { ?>
<footer class="mdl-mega-footer" role="contentinfo">
  <div class="mdl-mega-footer__middle-section">
    <?php dynamic_sidebar('sidebar-mega-footer'); ?>
  </div>
</footer>
<?php } ?>

<?php //if ( is_active_sidebar( 'sidebar-mini-footer' ) ) { ?>
<footer class="mdl-mini-footer mdl-grid" role="contentinfo">	
  <div class="copyrights mdl-cell mdl-cell--4-col">&copy;<?php echo date('Y'); ?> <?php bloginfo( 'name' ); ?></div>
  <div class="footerwidget mdl-cell mdl-cell--4-col"><?php dynamic_sidebar('sidebar-mini-footer'); ?></div>
  <div class="sociallinks mdl-cell mdl-cell--4-col">
  
  <?php get_template_part('templates/social'); ?>
  </div>
  
</footer>
<?php// } ?>
