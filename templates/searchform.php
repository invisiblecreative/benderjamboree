<h6>Search</h6>
<form role="search" method="get" class="search-form form-inline" action="<?= esc_url(home_url('/')); ?>"> 
  <div class="input-group">
    <input type="search" value="<?= get_search_query(); ?>" name="s" class="search-field form-control" placeholder="<?php _e('Press enter/return to search'); ?>" required>
   
  </div>
</form>