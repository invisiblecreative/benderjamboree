<div class="mdl-grid wt-bkg entry-content">
	<div class="mdl-cell mdl-cell--12-col">
		<h2 class="single-artist-title"><?php the_title(); ?></h2>
	</div>	
 	<div class="mdl-cell mdl-cell--5-col">
		
		<img src="<?php $img = get_field('artist_photo'); echo $img['url']; ?>" width="100%"/>
	</div>				
	<div class="mdl-cell mdl-cell--7-col">
		
		<?php if (get_field('artist_description')){?>
		<div class="artist-text">
			<p><?php the_field('artist_description'); ?></p>
		</div>
		<?php } ?>
<?php if(get_field('artist_video_url')){ ?>	
		<a class="popup-iframe" href="<?php the_field('artist_video_url'); ?>" title="<?php the_title(); ?>"><button class="mdl-button mdl-js-button mdl-button--raised" ><i class="material-icons">play_circle_outline</i> Watch Video</button></a>
	<?php } ?>	
	</div>
</div>
<?php get_template_part('templates/em-parts/em', 'artists'); ?>


