<?php while (have_posts()) : the_post(); ?>
  <article <?php post_class(); ?>>
    
     <?php if ( is_singular( 'packages_post_type' ) || is_singular( 'artists_post_type' ) ||  is_singular( 'pkg_addon' )) {
	     //do nothing
	    }else{?>
		 <header>    
		     <h1 class="entry-title"><?php the_title(); ?></h1>
		 </header>    
  <?php } ?>

     
     
     
     
      
      
    
    
  
<?php
if ( is_singular( 'packages_post_type' ) ) {
    	get_template_part('templates/single', 'package');
} elseif (is_singular( 'event_detail' )){
	get_template_part('templates/single', 'event-detail');
}elseif (is_singular( 'artists_post_type' )){
		get_template_part('templates/single', 'lineup');
}elseif (is_singular( 'pkg_addon' )){
		get_template_part('templates/single', 'addon');
}else{?>
	<div class="entry-content wt-bkg">
	<?php the_content(); ?>
	 </div>
<?php }?>
  
   
    <footer>
   
    </footer>
    
  </article>
<?php endwhile; ?>
