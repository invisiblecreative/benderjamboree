<?php
/**
 * Template Name: HTML Helper Template
 */
?>

<?php $proj = get_post_meta($post->ID, 'projected_lineup', true);

if (!empty($proj)) {
    $state = 'Projected Line-up';
    $moretxt ='To view the published line-up, go to the HTML-Helper page in the backend, and clear the field "projected_lineup" ';
    $varpass = 'em_projected_line_up';
}

else {
    $state = 'Published Line-up';
    $varpass = 'em_line_up';

}

?>


<h1> <?php echo $state; ?>  </h1>
<h4> <?php echo $moretxt; ?> </h4>


<div class="mdl-grid wt-bkg entry-content">

    <!--
    ~
    ~~
    ~~~
    ~~~~
    ~~~~~
    ~~~~~~
    ~~~~~~~
    ~~~~~~~~
    ~~~~~~~~~~	ARTIST LINEUP           CUT BELOW THIS COMMENT
    ~~~~~~~~~
    ~~~~~~~~
    ~~~~~~~
    ~~~~~~
    ~~~~~
    ~~~~
    ~~~
    ~~
    ~
    -->

    <?php while (have_posts()) : the_post();
        $artists = get_field($varpass, $GLOBALS['emid'] );

        if( $artists ){
            foreach (array_chunk($artists, 3, true) as $array)
            {
                echo '<div class="mcnTextContent">';
                foreach( $array as $post){
                    setup_postdata($post);
                    $image=get_field('artist_photo');
                    ?>
                    <div class="mcnTextContent artistholder">
                        <img class="mcnImage " src="<?php echo $image['sizes']['medium'];?>" width="200px" height="133px" />
                        <p class="artistname"><h4><a href="<?php the_permalink(); ?>"  style="text-align:center;display:block;"><?php the_title(); ?></a></h4></p>
                    </div>
                    <?php
                }
                echo '</div><div style="clear:both"></div>';
            }
        }
        wp_reset_postdata(); // IMPORTANT - reset the $post object ?>

    <?php endwhile; ?>

    <!--
    ~
    ~~
    ~~~
    ~~~~
    ~~~~~
    ~~~~~~
    ~~~~~~~
    ~~~~~~~~
    ~~~~~~~~~~	ARTIST LINEUP           CUT ABOVE THIS COMMENT
    ~~~~~~~~~
    ~~~~~~~~
    ~~~~~~~
    ~~~~~~
    ~~~~~
    ~~~~
    ~~~
    ~~
    ~
    -->
    <div style="width:100%; margin:20px 0px; clear:both;"></div>
    <!--
    ~
    ~~
    ~~~
    ~~~~
    ~~~~~
    ~~~~~~
    ~~~~~~~
    ~~~~~~~~
    ~~~~~~~~~~	PACKAGES              CUT BELOW THIS COMMENT
    ~~~~~~~~~
    ~~~~~~~~
    ~~~~~~~
    ~~~~~~
    ~~~~~
    ~~~~
    ~~~
    ~~
    ~
    -->



    <?php $packages = get_field('em_packages', $GLOBALS['emid'] ); ?>
    <?php if( $packages ){


        foreach (array_chunk($packages, 2, true) as $artarray)
        {
            echo '<div class="mcnTextContent row">';
            foreach( $artarray as $post){
                setup_postdata($post);
                $img=get_field('pkg_image');
                /*
                // set up display of ACF select field lable.
                $field_site_sales = get_field_object('site_status', 'option');
                $value_site_sales = get_field('site_status', 'option');
                $label_site_sales = $field_site_sales['choices'][ $value_site_sales ];

                $field_sales = get_field_object('pkg_sales_status');
                $value_sales = get_field('pkg_sales_status');
                $label_sales = $field_sales['choices'][ $value_sales ];
                // build booking url variable.
                $site_book_status = get_field('site_status', 'option');
                $trip = get_field('pkg_book_url').'/Booking/Reservation/Start?tripID='.get_field('em_rezmagic_trip_id', $GLOBALS['emid']);
                $book_url = get_field('pkg_book_url');
                if ($site_book_status == 'pre_sale' || $site_book_status == 'on_sale' ){
                        if ($value_sales == 'sold_out' ){
                        $book_url = '#';
                        }
                        else {
                        $book_url = $trip;
                    }
                    }
                elseif($value_sales == 'on_sale' || $value_sales == 'limited'){
                $book_url = $trip;
                }
                else {
                $book_url = '#';
                }
                */
                $field_location = get_field_object('pkg_location');
                $value_location = get_field('pkg_location');
                $label_location = $field_location['choices'][ $value_location ];
                $field_bedding = get_field_object('bedding_preference');
                $value_bedding = get_field('bedding_preference');
                $label_bedding = $field_bedding['choices'][ $value_bedding ];
                $field_smoking = get_field_object('pkg_smoking_preference');
                $value_smoking = get_field('pkg_smoking_preference');
                $label_smoking = $field_smoking['choices'][ $value_smoking ];
                $field_occupancy = get_field_object('pkg_occupancy');
                $value_occupancy = get_field('pkg_occupancy');
                $label_occupancy = $field_occupancy['choices'][ $value_occupancy ];


                $eventID = $GLOBALS['emid'];
                //EVENT DETAIL FIELD VALUES AND LABLES
                $field_site_sales = get_field_object('em_sales_status', $eventID);
                $value_site_sales = get_field('em_sales_status', $eventID);
                $label_site_sales = $field_site_sales['choices'][ $value_site_sales ];
                $site_book_status = $value_site_sales;
//PACKAGE FIELD VALUES AND LABLES
                $field_package_sales = get_field_object('pkg_sales_status');
                $value_package_sales = get_field('pkg_sales_status');
                $label_package_sales = $field_package_sales['choices'][ $value_package_sales ];
                $package_book_status = $value_package_sales;
// Setting up base booking url
                $pkg_book_url = get_field('pkg_book_url');
                $book_url = get_field('em_booking_url', $eventID);
// SETTING UP THE TRIP ID REZMAGIC
                if($site_book_status == 'prebook_on_sale') {
                    $tripID = get_field('em_prebooking_trip_id', $eventID);
                } else {
                    $tripID = get_field('em_rezmagic_trip_id', $eventID);
                }
// BOOK BUTTON URL BUILD
                $trip = "";
                if($pkg_book_url != "") {
                    $trip = $pkg_book_url;
                } else{
                    $trip = $book_url . '/Booking/Reservation/Start?tripID=' . $tripID;
                }
// BOOK BUTTON LINK
                if ($site_book_status == 'prebook_now' || $site_book_status == 'on_sale' || $site_book_status == 'prebook_on_sale' ){
                    if ($package_book_status == 'sold_out' ){
                        $book_link = '#';
                    } else {
                        $book_link = $trip;
                    }
                } else {
                    $book_link = '#';
                }
// BOOK BUTTON TEXT
                $sitewide_sales_text = get_field('em_sitewide_sales_text', $eventID);
                $book_btn_text = "";
                if ( $site_book_status == 'prebook_now' ||
                    $site_book_status == 'on_sale' ||
                    $site_book_status == 'sold_out' ||
                    $site_book_status == 'closed' ||
                    $site_book_status == 'prebook_on_sale' ){
                    if (
                        $site_book_status == 'prebook_now' ||
                        $site_book_status == 'on_sale' ||
                        $site_book_status == 'prebook_on_sale'
                    ){
                        if(
                            $package_book_status == 'sold_out' ||
                            $package_book_status == 'limited'
                        ){
                            $book_btn_text = $label_package_sales;
                        } else {
                            $book_btn_text = $label_site_sales;
                        }
                    } else {
                        $book_btn_text = $label_site_sales;
                    }
                } elseif (
                    $site_book_status == 'prebook_soon' ||
                    $site_book_status == 'prebook_announce'
                ){
                    $book_btn_text = $sitewide_sales_text;
                } ?>


                <div class="mcnTextContent pkgcontainer">
                    <a  href="<?php the_permalink(); ?>" >
                        <img class="mcnImage" src="<?php echo $img['sizes']['medium'];?>" width="275px" height="183px" />
                        <h4 class="packagetitle"><?php the_title(); ?></h4>
                    </a>

                    <div class="pkg_icons">
                        <span><img src="https://gallery.mailchimp.com/62512c4fbdf2f02a95c9fb1b4/images/f5a23d20-a330-455b-b04c-319099d9b672.png" width="30" height="30"/> <?php echo $label_location; ?></span>
                    </div>
                    <div class="pkg_icons">
                        <span><img src="https://gallery.mailchimp.com/62512c4fbdf2f02a95c9fb1b4/images/61f85c42-492e-4747-addb-0d2bdc090ae0.png" width="30" height="30"/> <?php echo $label_bedding; ?></span>
                    </div>
                    <div class="pkg_icons">
                        <span><img src="https://gallery.mailchimp.com/62512c4fbdf2f02a95c9fb1b4/images/1e408b5c-b315-43e3-aa5e-f2f6bbdd76c1.png" width="30" height="30"/> <?php echo $label_smoking; ?></span>
                    </div>
                    <div class="pkg_icons">
                        <span><img src="https://gallery.mailchimp.com/62512c4fbdf2f02a95c9fb1b4/images/8b51dc14-12a7-444a-be5f-1b89c5a33726.png" width="30" height="30"/> <?php echo $label_occupancy; ?></span>
                    </div>


                    <p><span class="package-price">$<?php the_field('pkg_price_pp'); ?> <span>per person</span></span></p>

                    <div class="book-btn-container book-btn-container-<?php echo $site_book_status;?>  book-btn-container-<?php echo $package_book_status;?> mdl-shadow--2dp">
                        <a
                                class="book-btn <?php echo $site_book_status; ?> book-btn-<?php echo $package_book_status;?>"
                                href="<?php echo $book_link; ?>" target="_blank" ><?php echo $book_btn_text; ?></a>
                    </div>
                </div>

                <?php
            }
            echo '</div><!-- close row --><div style="clear:both"></div>';
        }

        wp_reset_postdata();
    } ?>

    <!--
    ~
    ~~
    ~~~
    ~~~~
    ~~~~~
    ~~~~~~
    ~~~~~~~
    ~~~~~~~~
    ~~~~~~~~~~	PACKAGES              CUT ABOVE THIS COMMENT
    ~~~~~~~~~
    ~~~~~~~~
    ~~~~~~~
    ~~~~~~
    ~~~~~
    ~~~~
    ~~~
    ~~
    ~
    -->

</div>