<?php
/**
 * Template Name: Lineup Template
 */
?>

<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/em-parts/em', 'artists'); ?>
<?php endwhile; ?>
