<div class="mdl-grid wt-bkg entry-content">
<div class="mdl-cell mdl-cell--12-col">
<?php get_template_part('templates/page', 'header'); ?>
</div>
<div class="mdl-cell mdl-cell--12-col">
<?php if (!have_posts()) : ?>
  <div class="alert alert-warning">
    <?php _e('Sorry, no results were found.', 'sage'); ?>
  </div>
 <div class="search-form-wrap">
		<?php get_template_part('templates/searchform'); ?>
	</div>
<?php endif; ?>

<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/content', 'search'); ?>
<?php endwhile; ?>

<?php the_posts_navigation(); ?>
</div>
</div>